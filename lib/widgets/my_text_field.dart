import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../utils/styles.dart';

Widget MyTextField({
  required BuildContext context,
  required String hintText,
  required String labelTxt,
  required IconData icon,
  required TextInputType inputType,
  required String controlName,
  required List<TextInputFormatter> inputFormatter,
  required Map<String, String> Function(FormControl) validationMessages,
  bool? readOnly,
  bool? obscure,
  VoidCallback? onEditingComplete,
  Iterable<String>? autofill,
}) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 10),
    child: ReactiveTextField(
      obscureText: obscure==null?false:obscure,
      obscuringCharacter:"*",
      onSubmitted: onEditingComplete,
      readOnly: readOnly==null?false:readOnly,
      autofillHints: autofill,
      inputFormatters: inputFormatter,
      showErrors: (control) =>
          control.invalid && control.touched && control.dirty,
      formControlName: controlName,
      validationMessages: validationMessages,
      keyboardAppearance: Brightness.dark,
      style: TextStyle(
        color: Styles.textColor(context),
        fontSize: 15,
      ),
      keyboardType: inputType,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: Icon(
          icon,
          size: 15,
          color: Styles.iconColor(context),
        ),
        contentPadding: EdgeInsets.all(20),
        labelText: labelTxt,
        hintText: hintText,
        labelStyle: TextStyle(
          color: Styles.textColor(context),
          fontSize: 15,
        ),
        hintStyle: TextStyle(
          color: Styles.subTextColor(context),
          fontSize: 15,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Styles.fieldColor(context),
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(18),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Styles.selectedItemColor(context),
            width: 1.8,
          ),
          borderRadius: BorderRadius.circular(18),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
            width: 1.8,
          ),
          borderRadius: BorderRadius.circular(18),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(18),
        ),
      ),
    ),
  );
}
