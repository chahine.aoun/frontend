
import 'package:flutter/material.dart';
import 'package:frontend/utils/styles.dart';
import 'package:intl/intl.dart';

class TransactionRep extends StatelessWidget {
  final bool icon;
  final String title;
  final double amount;
  final DateTime date;

   TransactionRep({
     Key? key,
    required this.amount,
    required this.date,
    required this.icon,
    required this.title,
  }) : super(key: key);
  final dateFormat = DateFormat('EEE, MMM d, h:mm a');
  @override
  Widget build(BuildContext context) {
    final time = dateFormat.format(date);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0,),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 45.0,
                width: 45.0,
                decoration: BoxDecoration(
                  color: Styles.AccentColor(context),
                  borderRadius: BorderRadius.circular(18.0),
                ),
                child: Center(
                  child:Icon(icon?Icons.remove_rounded:Icons.add_rounded,
                  color: icon?Colors.redAccent:Colors.greenAccent,
                  size: 35.0,
                 ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Text(
                          icon?"Sent to:    ":"Sent from: ",
                          style: TextStyle(color: Styles.subTextColor(context),
                            fontSize: 11,
                          ),
                        ),
                        Text(
                          title,
                          style:TextStyle(color: Styles.textColor(context),fontSize: 17.0,),
                        ),
                      ],
                    ),
                    Text(
                      time,
                      style: TextStyle(color: Styles.subTextColor(context),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Text(
                '\$ $amount',
                style:TextStyle(color: Styles.textColor(context),
                fontSize: 18.0,),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
