import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/screens/authentication/create_bank_account.dart';
import 'package:frontend/screens/main/bank_statement.dart';
import 'package:get/get.dart';

import '../screens/authentication/login_page.dart';
import '../screens/main/accountInformation.dart';
import '../utils/styles.dart';

Widget MyDrawer({
  required BuildContext context,
}) {
  final _storage = const FlutterSecureStorage();
  String name = "";
  String email = "";
  Future<void> getInfo() async {
    name = (await _storage.read(key: 'name'))!;
    email = (await _storage.read(key: 'email'))!;
  }

  return Drawer(
    backgroundColor: Styles.bgColor(context),
    shape: RoundedRectangleBorder(
      side: new BorderSide(
        color: Colors.transparent,
      ),
      borderRadius: BorderRadius.horizontal(
        right: Radius.circular(10),
      ),
    ),
    width: Get.width * 0.6,
    child: ListView(
      // Remove padding
      padding: EdgeInsets.zero,
      children: [
        FutureBuilder<void>(
          future: getInfo(),
          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
            return UserAccountsDrawerHeader(
              accountName: Text(
                name,
                style: TextStyle(
                  color: Styles.titleColor(context),
                ),
              ),
              accountEmail: Text(
                email,
                style: TextStyle(
                  color: Styles.titleColor(context),
                ),
              ),
              decoration: BoxDecoration(
                color: Styles.AccentColor(context),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                ),
              ),
            );
          },
        ),
        ListTile(
          leading: Icon(Icons.home_filled),
          title: Text(
            'Home',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        ListTile(
          leading: Icon(Icons.person_rounded),
          title: Text(
            'Profile',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
          onTap: () {
            Navigator.of(context).pop();
            Get.to(
              () => AccountInformation(),
            );
          },
        ),
        ListTile(
          leading: Icon(Icons.wallet_rounded),
          title: Text(
            'Wallet',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.add_rounded),
          title: Text(
            'New Bank Account',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
          onTap: () {
            Navigator.of(context).pop();
            Get.to(
              () => CreateBankAccount(),
            );
          },
        ),
        Divider(
          color: Styles.dividerColor(context),
        ),
        ListTile(
          leading: Icon(Icons.description),
          title: Text(
            'Bank Statement',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
          onTap: () {
            Navigator.of(context).pop();
            Get.to(
              () => BankStatement(),
            );
          },
        ),
        Divider(
          color: Styles.dividerColor(context),
        ),
        ListTile(
          title: Text(
            'Log out',
            style: TextStyle(
              color: Styles.titleColor(context),
            ),
          ),
          leading: Icon(Icons.exit_to_app),
          onTap: () {
            Navigator.of(context).pop();
            _storage.deleteAll();
            Get.offAll(() => LoginPage());
          },
        ),
      ],
    ),
  );
}
