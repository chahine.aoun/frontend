import 'package:flutter/material.dart';
import 'package:frontend/models/bankAccounts/bankAccounts.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';

class MyCard extends StatelessWidget {
  final BankAccounts acc;
  const MyCard({Key? key, required this.acc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return FittedBox(
      child: SizedBox(
        height: size.height * 0.23,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: size.width * 0.67,
              padding: const EdgeInsets.fromLTRB(16, 10, 0, 20),
              decoration: BoxDecoration(
                borderRadius:
                    const BorderRadius.horizontal(left: Radius.circular(15)),
                color: acc.active ? Styles.greenColor : Color(0xFF7be8b8),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('\$' + acc.balance.toPrecision(3).toString(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: acc.active ? Colors.white : Colors.black87)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('ACCOUNT NUMBER',
                          style: TextStyle(
                              color: acc.active
                                  ? Colors.white.withOpacity(0.5)
                                  : Colors.black.withOpacity(0.8),
                              fontSize: 12)),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Row(
                        children: [
                          Text(
                            acc.code.toString(),
                            style: TextStyle(
                              color: acc.active ? Colors.white : Colors.black87,
                              fontSize: 15,
                            ),
                          ),
                          Text(
                            "/"+acc.type.type,
                            style: TextStyle(
                              color: acc.active  ? Colors.white.withOpacity(0.5)
                                  : Colors.black.withOpacity(0.8),
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: size.width * 0.27,
              padding: const EdgeInsets.fromLTRB(20, 10, 0, 20),
              decoration: BoxDecoration(
                borderRadius:
                    const BorderRadius.horizontal(right: Radius.circular(15)),
                color: acc.active ? Styles.yellowColor : Styles.greenColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: acc.active ? Styles.greenColor : Colors.white,
                    ),
                    child: Icon(
                        acc.active
                            ? Icons.attach_money_rounded
                            : Icons.money_off_csred_rounded,
                        color: acc.active ? Colors.white : Styles.greenColor,
                        size: 20),
                  ),
                  const Spacer(),
                  Text('STATUS',
                      style: TextStyle(
                        fontSize: 12,
                        color: acc.active ? Colors.black : Colors.white,
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(acc.active ? 'Active' : 'Inactive',
                      style: TextStyle(
                        fontSize: 15,
                        color: acc.active ? Colors.black : Colors.white,
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
