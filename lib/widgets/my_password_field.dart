import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../utils/styles.dart';

class MyPasswordField extends StatefulWidget {
  const MyPasswordField({
    Key? key,
    required this.hintText,
    required this.labelTxt,
    required this.inputType,
    required this.controlName,
    required this.validationMsgs,
  }) : super(key: key);
  final String hintText;
  final String labelTxt;
  final TextInputType inputType;
  final String controlName;
  final Map<String, String> Function(FormControl) validationMsgs;

  @override
  _PasswordFieldState createState() {
    return _PasswordFieldState();
  }
}

class _PasswordFieldState extends State<MyPasswordField> {
  bool obscure = true;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: ReactiveTextField(
        showErrors: (control)=>control.invalid && control.touched && control.dirty,
        obscureText: obscure,
        formControlName: widget.controlName,
        validationMessages: widget.validationMsgs,
        keyboardAppearance: Brightness.dark,
        style: TextStyle(
          color: Styles.textColor(context),
          fontSize: 15,
        ),
        keyboardType: widget.inputType,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.lock_rounded,
            size: 15,
            color: Styles.iconColor(context),
          ),
          suffixIcon: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: _toggle,
              child: Icon(
                !obscure ? Icons.visibility : Icons.visibility_off,
                color: Styles.iconColor(context),
              ),
            ),
          ),
          contentPadding: EdgeInsets.all(20),
          labelText: widget.labelTxt,
          hintText: widget.hintText,
          labelStyle: TextStyle(
            color: Styles.textColor(context),
            fontSize: 15,
          ),
          hintStyle: TextStyle(
            color: Styles.subTextColor(context),
            fontSize: 15,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Styles.fieldColor(context),
              width: 1.5,
            ),
            borderRadius: BorderRadius.circular(18),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Styles.selectedItemColor(context),
              width: 1.8,
            ),
            borderRadius: BorderRadius.circular(18),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red,
              width: 1.8,
            ),
            borderRadius: BorderRadius.circular(18),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.red,
              width: 1.5,
            ),
            borderRadius: BorderRadius.circular(18),
          ),
        ),
      ),
    );
  }

  void _toggle() {
    setState(() {
      obscure = !obscure;
    });
  }
}
