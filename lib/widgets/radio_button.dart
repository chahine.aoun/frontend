import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';

class RadioButton extends StatefulWidget {
  @override
  _RadioState createState() {
    return _RadioState();
  }
}

class _RadioState extends State<RadioButton> {
  Object? val = 'male';
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Radio(
          value: "male",
          groupValue: val,
          onChanged: (value) {
            setState(() {
              val = value;
            });
            updateValue(value.toString());
          },
          activeColor: Styles.selectedItemColor(context),
        ),
        Text("Male"),
        Radio(
          value: 'female',
          groupValue: val,
          onChanged: (value) {
            setState(() {
              val = value;
            });
            updateValue(value.toString());
          },
          activeColor: Styles.selectedItemColor(context),
        ),
        Text("Female"),
      ],
    );
  }

  void updateValue(String value) {
    var c = Get.find<FieldController>();
    c.fieldFormGroup.updateValue({
      "gender": value,
      "name": c.fieldFormGroup.value["name"],
      "lastName": c.fieldFormGroup.value["lastName"],
    });
  }
}
