import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/screens/authentication/registration/useless.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/utils/view_model.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ViewModel(),
        ),
      ],
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
        home: Useless(),
        theme: ThemeData(
          fontFamily: 'DMSans',
          primaryColor: Styles.primaryColor,
          backgroundColor: Styles.primaryColor,
          accentColor: Styles.blueColor,
          brightness:Brightness.light,
        ),
       darkTheme: ThemeData(
         fontFamily: 'DMSans',
         primaryColor: Styles.primaryColor,
         backgroundColor: Styles.primaryColor,
         accentColor: Styles.blueColor,
         brightness:Brightness.dark,
       ),
       themeMode: ThemeMode.light,
        initialBinding: BindingsBuilder(() {
          Get.lazyPut(()=>FieldController());
        }),
      ),
    );
  }
}
