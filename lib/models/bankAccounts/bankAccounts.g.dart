// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bankAccounts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankAccounts _$BankAccountsFromJson(Map<String, dynamic> json) => BankAccounts(
      code: json['code'] as int,
      balance: (json['balance'] as num).toDouble(),
      active: json['active'] as bool,
      transactionCounter: json['transactionCounter'] as int,
      bank: json['bank'] as String,
      agency: json['agency'] as String,
      type: BankAccountType.fromJson(json['type'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BankAccountsToJson(BankAccounts instance) =>
    <String, dynamic>{
      'code': instance.code,
      'balance': instance.balance,
      'active': instance.active,
      'transactionCounter': instance.transactionCounter,
      'bank': instance.bank,
      'agency': instance.agency,
      'type': instance.type,
    };
