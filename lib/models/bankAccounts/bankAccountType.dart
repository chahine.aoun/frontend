import 'package:json_annotation/json_annotation.dart';

part 'bankAccountType.g.dart';

@JsonSerializable()
class BankAccountType {
  BankAccountType(this.type,
      this.transactionFee,
      this.interestRate,
      this.transactionLimit,
      this.minBalance);

  final String type;
  final double transactionFee;
  final double interestRate;
  final int transactionLimit;
  final int minBalance;

  factory BankAccountType.fromJson(Map<String, dynamic> json) => _$BankAccountTypeFromJson(json);
  Map<String, dynamic> toJson() => _$BankAccountTypeToJson(this);
}
