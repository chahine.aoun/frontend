import 'package:json_annotation/json_annotation.dart';

import 'bankAccountType.dart';

part 'bankAccounts.g.dart';

@JsonSerializable()
class BankAccounts {
  BankAccounts( {
    required this.code,
    required this.transactionCounter,
    required this.balance,
    required this.active,
    required this.bank,
    required this.agency,
    required this.type,
  });

  final int code;
  final double balance;
  final bool active;
  final int transactionCounter;
  final String bank;
  final String agency;
  final BankAccountType type;

  factory BankAccounts.fromJson(Map<String, dynamic> json) => _$BankAccountsFromJson(json);
  Map<String, dynamic> toJson() => _$BankAccountsToJson(this);
}
