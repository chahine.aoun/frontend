// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bankAccountType.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankAccountType _$BankAccountTypeFromJson(Map<String, dynamic> json) =>
    BankAccountType(
      json['type'] as String,
      (json['transactionFee'] as num).toDouble(),
      (json['interestRate'] as num).toDouble(),
      json['transactionLimit'] as int,
      json['minBalance'] as int,
    );

Map<String, dynamic> _$BankAccountTypeToJson(BankAccountType instance) =>
    <String, dynamic>{
      'type': instance.type,
      'transactionFee': instance.transactionFee,
      'interestRate': instance.interestRate,
      'transactionLimit': instance.transactionLimit,
      'minBalance': instance.minBalance,
    };
