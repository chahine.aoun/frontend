import 'package:json_annotation/json_annotation.dart';

part 'userResponse.g.dart';

@JsonSerializable()
class UserResponse {
  UserResponse({

    required this.firstName,
    required this.lastName,
    required this.email,
    required this.phoneNumber,
    required this.gender,
    required this.typeOfId,
    required this.idNumber,
    required this.referralCode,
    required this.expirationDate,
    required this.BirthDate,
    required this.countryOfResidence,
    required this.address,
  });


  String firstName;
  String lastName;
  String email;
  String phoneNumber;
  String gender;
  String typeOfId;
  String idNumber;
  String referralCode;
  String expirationDate;
  String BirthDate;
  String countryOfResidence;
  String address;


  factory UserResponse.fromJson(Map<String, dynamic> json) => _$UserResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UserResponseToJson(this);
  String toJsonString()=>jsonString(this);

  String jsonString(UserResponse instance){
    String s='{';
    s=s+'"firstName":"'+instance.firstName+'",';
    s=s+'"lastName":"'+instance.lastName+'",';
    s=s+ '"email":"'+instance.email+'",';
    s=s+ '"phoneNumber":"'+instance.phoneNumber+'",';
    s=s+'"typeOfID":"'+instance.typeOfId+'",';
    s=s+'"idNumber":"'+instance.idNumber+'",';
    s=s+ '"referralCode":"'+instance.referralCode+'",';
    s=s+ '"expirationDate":"'+instance.expirationDate+'",';
    s=s+ '"birthday":"'+instance.BirthDate+'",';
    s=s+'"gender":"'+instance.gender+'",';
    s=s+ '"countryOfResidence":"'+instance.countryOfResidence+'",';
    s=s+ '"address":"'+instance.address+'"}';
    return s;
  }


}