// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      email: json['email'] as String,
      phoneNumber: json['phoneNumber'] as String,
      gender: json['gender'] as String,
      typeOfId: json['typeOfId'] as String,
      idNumber: json['idNumber'] as String,
      referralCode: json['referralCode'] as String,
      expirationDate: json['expirationDate'] as String,
      BirthDate: json['BirthDate'] as String,
      countryOfResidence: json['countryOfResidence'] as String,
      address: json['address'] as String,
    );

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'gender': instance.gender,
      'typeOfId': instance.typeOfId,
      'idNumber': instance.idNumber,
      'referralCode': instance.referralCode,
      'expirationDate': instance.expirationDate,
      'BirthDate': instance.BirthDate,
      'countryOfResidence': instance.countryOfResidence,
      'address': instance.address,
    };
