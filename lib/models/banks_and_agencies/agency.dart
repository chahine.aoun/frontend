import 'package:json_annotation/json_annotation.dart';


part 'agency.g.dart';

@JsonSerializable()
class Agency {
  Agency({
    required this.code,
    required this.name,
    required this.address,
  });

  String code;
  String name;
  String address;

  factory Agency.fromJson(Map<String, dynamic> json) => _$AgencyFromJson(json);
  Map<String, dynamic> toJson() => _$AgencyToJson(this);
}
