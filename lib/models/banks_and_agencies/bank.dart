import 'package:json_annotation/json_annotation.dart';



part 'bank.g.dart';

@JsonSerializable()
class Bank {
  Bank({
    required this.swiftCode,
    required this.name,
    required this.address,
  });

  String swiftCode;
  String name;
  String address;

  factory Bank.fromJson(Map<String, dynamic> json) => _$BankFromJson(json);
  Map<String, dynamic> toJson() => _$BankToJson(this);
}
