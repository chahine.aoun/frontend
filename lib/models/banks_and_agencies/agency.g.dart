// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agency.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Agency _$AgencyFromJson(Map<String, dynamic> json) => Agency(
      code: json['code'] as String,
      name: json['name'] as String,
      address: json['address'] as String,
    );

Map<String, dynamic> _$AgencyToJson(Agency instance) => <String, dynamic>{
      'code': instance.code,
      'name': instance.name,
      'address': instance.address,
    };
