

import 'package:json_annotation/json_annotation.dart';



part 'transaction.g.dart';

@JsonSerializable()
class Transaction {
  Transaction({
    required this.date,
    required this.sender,
    required this.receiver,
    required this.amount,
    required this.sent,
  });

  DateTime date;
  int sender;
  int receiver;
  double amount;
  bool sent;

  factory Transaction.fromJson(Map<String, dynamic> json) => _$TransactionFromJson(json);
  Map<String, dynamic> toJson() => _$TransactionToJson(this);
}
