// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) => Transaction(
      date: DateTime.parse(json['date'] as String),
      sender: json['sender'] as int,
      receiver: json['receiver'] as int,
      amount: (json['amount'] as num).toDouble(),
      sent: json['sent'] as bool,
    );

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'date': instance.date.toIso8601String(),
      'sender': instance.sender,
      'receiver': instance.receiver,
      'amount': instance.amount,
      'sent': instance.sent,
    };
