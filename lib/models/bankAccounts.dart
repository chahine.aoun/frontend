import 'package:json_annotation/json_annotation.dart';

part 'bankAccounts.g.dart';

@JsonSerializable()
class BankAccounts {
  BankAccounts({
    required this.code,
    required this.balance,
    required this.active,
    required this.type,
    required this.others
  });

  final int code;
  final double balance;
  final bool active;
  final String type;
  final List<Object> others;

  factory BankAccounts.fromJson(Map<String, dynamic> json) => _$BankAccountsFromJson(json);
  Map<String, dynamic> toJson() => _$BankAccountsToJson(this);
  String toJsonString()=>jsonString(this);

  String jsonString(BankAccounts instance){
    String s='{';
    s=s+'"code":"'+instance.code.toString()+'",';
    s=s+'"balance":"'+instance.balance.toString()+'",';
    s=s+ '"active":"'+instance.active.toString()+'",';
    s=s+ '"type":"'+instance.type+'"}';
    return s;
  }
}
