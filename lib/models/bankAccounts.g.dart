// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bankAccounts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankAccounts _$BankAccountsFromJson(Map<String, dynamic> json) => BankAccounts(
      code: json['code'] as int,
      balance: json['balance'] as double,
      active: json['active'] as bool,
      type: json['type'] as String,
      others: json['others'] as List<Object>,
    );

Map<String, dynamic> _$BankAccountsToJson(BankAccounts instance) =>
    <String, dynamic>{
      'code': instance.code,
      'balance': instance.balance.toString(),
      'active': instance.active,
      'type': instance.type,
      'others': instance.others,
    };
