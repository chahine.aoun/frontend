// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      email: json['email'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      password: json['password'] as String,
      phoneNumber: json['phoneNumber'] as String,
      typeOfId: json['typeOfId'] as String,
      idNumber: json['idNumber'] as String,
      referralCode: json['referralCode'] as String,
      expirationDate: json['expirationDate'] as String,
      BirthDate: json['BirthDate'] as String,
      gender: json['gender'] as String,
      countryOfResidence: json['countryOfResidence'] as String,
      address: json['address'] as String,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'email': instance.email,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'password': instance.password,
      'phoneNumber': instance.phoneNumber,
      'typeOfId': instance.typeOfId,
      'idNumber': instance.idNumber,
      'referralCode': instance.referralCode,
      'expirationDate': instance.expirationDate,
      'BirthDate': instance.BirthDate,
      'gender': instance.gender,
      'countryOfResidence': instance.countryOfResidence,
      'address': instance.address,
    };
