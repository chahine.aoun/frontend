
import 'package:json_annotation/json_annotation.dart';

part 'data.g.dart';

@JsonSerializable()
class Data {
  Data({
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.password,
    required this.phoneNumber,
    required this.typeOfId,
    required this.idNumber,
    required this.referralCode,
    required this.expirationDate,
    required this.BirthDate,
    required this.gender,
    required this.countryOfResidence,
    required this.address,
  });

  String email;
  String firstName;
  String lastName;
  String password;
  String phoneNumber;
  String typeOfId;
  String idNumber;
  String referralCode;
  String expirationDate;
  String BirthDate;
  String gender;
  String countryOfResidence;
  String address;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);
  String toJsonString()=>jsonString(this);


  String jsonString(Data instance){
    String s='{';
    s=s+'"firstName":"'+instance.firstName+'",';
    s=s+'"lastName":"'+instance.lastName+'",';
    s=s+ '"email":"'+instance.email+'",';
    s=s+ '"phoneNumber":"'+instance.phoneNumber+'",';
    s=s+ '"password":"'+instance.password+'",';
    s=s+'"typeOfID":"'+instance.typeOfId+'",';
    s=s+'"idNumber":"'+instance.idNumber+'",';
    s=s+ '"referralCode":"'+instance.referralCode+'",';
    s=s+ '"expirationDate":"'+instance.expirationDate+'",';
    s=s+ '"birthday":"'+instance.BirthDate+'",';
    s=s+'"gender":"'+instance.gender+'",';
    s=s+ '"countryOfResidence":"'+instance.countryOfResidence+'",';
    s=s+ '"address":"'+instance.address+'"}';
    return s;
  }

}