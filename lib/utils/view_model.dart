import 'package:flutter/material.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewModel extends ChangeNotifier {
  late bool _isDark ;
  bool get isDark => _isDark;

  Future<bool?> getPref(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.reload();
    bool? a = prefs.getBool('isDark');
    if (a != null) {
      _isDark = a;
    } else {
      _isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;
    }
    Get.changeTheme(_isDark?ThemeData(
      fontFamily: 'DMSans',
      primaryColor: Styles.primaryColor,
      backgroundColor: Styles.primaryColor,
      accentColor: Styles.blueColor,
      brightness:Brightness.dark,
    ):ThemeData(
      fontFamily: 'DMSans',
      primaryColor: Styles.primaryColor,
      backgroundColor: Styles.primaryColor,
      accentColor: Styles.blueColor,
      brightness:Brightness.light,
    ));
    return a;
  }

  setPref(bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isDark', value);
    _isDark = value;
    prefs.reload();
  }

  setToDark(BuildContext context) async {
    await getPref(context).then((value) => _isDark = value ?? false);
    notifyListeners();
  }
}
