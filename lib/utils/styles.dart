import 'package:flutter/material.dart';
import 'package:frontend/utils/view_model.dart';
import 'package:provider/provider.dart';


class Styles{
  static Color primaryColor = const Color(0xFF161D28);
  static Color accentColor = const Color(0xFF030C10);
  static Color primaryWithOpacityColor = const Color(0xFF212E3E);
  static Color yellowColor = const Color(0xFFDFE94B);
  static Color greenColor = const Color(0xFF024751);
  static Color greyColor = const Color(0xFFE6E8E8);
  static Color whiteColor = Colors.white;
  static Color buttonColor = const Color(0xFF4C66EE);
  static Color blueColor = const Color(0xFF4BACF7);


  static getTheme(BuildContext context) {
    final vm = context.watch<ViewModel>();
    return vm.isDark;
  }
  static getSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static Color bgColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryColor : Styles.whiteColor.withOpacity(1);
  }

  static Color cardColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.greenColor : Styles.accentColor;
  }

  static Color cardColor2(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.accentColor : Styles.greenColor;
  }

  static Color navbarColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryWithOpacityColor : Styles.whiteColor;
  }

  static Color selectedItemColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.blueColor : Styles.greenColor;
  }

  static Color textColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor : Styles.primaryColor;
  }

  static Color fieldColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor.withOpacity(0.5) : Colors.grey.withOpacity(0.5);
  }

  static Color headerColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryColor : Styles.greenColor;
  }

  static Color dividerColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.greyColor.withOpacity(0.2) : Colors.grey.withOpacity(0.6);
  }

  static Color subTextColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor.withOpacity(0.7) : Styles.primaryColor.withOpacity(0.7);
  }

  static Color iconColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor.withOpacity(0.2) : Colors.grey.withOpacity(0.7);
  }

  static Color headerColor2(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryWithOpacityColor : Styles.greenColor;
  }

  static Color AccentColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryWithOpacityColor : Colors.grey.shade50;
  }

  static Color accentColor2(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.primaryWithOpacityColor : Colors.transparent;
  }

  static Color notificationColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor : Styles.primaryColor;
  }

  static Color thumbColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.blueColor : Styles.greyColor;
  }

  static Color activeColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor : Styles.greyColor;
  }

  static Color titleColor(BuildContext context) {
  final vm = context.watch<ViewModel>();
  return vm.isDark == true ? Styles.whiteColor : Styles.greenColor;
  }
  static Color whiteAndBlack(BuildContext context) {
    final vm = context.watch<ViewModel>();
    return vm.isDark == true ? Colors.white : Colors.black;
  }
  static Color linkColor(BuildContext context) {
    final vm = context.watch<ViewModel>();
    return vm.isDark == true ? Colors.blue : Styles.blueColor;
  }
  static AssetImage splashImage(BuildContext context) {
    final vm = context.watch<ViewModel>();
    return vm.isDark == true ? new AssetImage('assets/images/splash_logo.png')
        :new AssetImage('assets/images/splash_logo_light.png');
  }
}
