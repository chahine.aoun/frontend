import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/models/bankAccounts/bankAccounts.dart';
import 'package:frontend/models/transactions/transaction.dart';

import '../models/banks_and_agencies/agency.dart';
import '../models/banks_and_agencies/bank.dart';
import '../models/user_response/userResponse.dart';

class Api {
  final Dio api = Dio(
    BaseOptions(
      baseUrl: 'http://10.0.2.2:8082/app',
      connectTimeout: 15000,
      receiveTimeout: 15000,
      validateStatus: (code) {
        if (code! == 403) {
          print('forbidden');
          return false;
        }
        return true;
      },
    ),
  );

  String? accessToken = "";
  final _storage = const FlutterSecureStorage();

  Api() {
    api.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      options.headers['Authorization'] = 'Bearer $accessToken';
      return handler.next(options);
    }, onError: (DioError error, handler) async {
      if (error.response?.statusCode == 403) {
        if (await _storage.containsKey(key: 'refreshToken')) {
          if (await refreshToken()) {
            return handler.resolve(await _retry(error.requestOptions));
          }
        }
      }
      return handler.next(error);
    }));
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return api.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }

  Future<bool> refreshToken() async {
    final refreshToken = await _storage.read(key: 'refreshToken');
    FormData formData = FormData.fromMap({
      'token': 'Bearer $refreshToken',
    });
    final response = await api.post('/account/token/refresh', data: formData);
    if (response.statusCode == 200) {
      accessToken = response.headers.value("access_token").toString();
      return true;
    } else {
      // refresh token is wrong
      accessToken = null;
      _storage.deleteAll();
      return false;
    }
  }

  Future<int> count() async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get(
      '/account/bankAccount/number',
    );
    return res.data;
  }

  Future<String> createBankAccount(
      String type, String swiftCode, String agencyCode) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.post('/account/bankAccount', queryParameters: {
      "type": type,
      "swift code": swiftCode,
      "agency code": agencyCode,
    });
    return res.toString();
  }

  Future<List<BankAccounts>> loadBankAccounts() async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get(
      '/account/bankAccount/all',
    );
    List<BankAccounts> accounts = [];
    for (var i in res.data) accounts.add(BankAccounts.fromJson(i));
    accounts.sort((first, second) {
      if ((first.balance < second.balance))
        return 1;
      else if ((first.balance > second.balance) & first.active)
        return -1;
      else
        return 0;
    });
    return accounts;
  }

  Future<List<Transaction>> loadTransactions() async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get(
      '/account/transaction',
    );
    List<Transaction> transactions = [];
    for (var i in res.data) transactions.add(Transaction.fromJson(i));
    transactions.sort((first, second) {
      if (first.date.isAfter(second.date))
        return -1;
      else if (second.date.isAfter(first.date))
        return 1;
      else
        return 0;
    });
    return transactions;
  }

  Future<void> deleteBankAccount(int code) async {
    accessToken = await _storage.read(key: 'accessToken');
    await api.delete('/account/bankAccount', queryParameters: {
      "code": code,
    });
  }

  Future<String> checkPinCode(String pin) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get('/account/pin/check', queryParameters: {
      "pin": pin,
    });
    return res.toString();
  }

  Future<String> checkBankAccount(String code) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res =
        await api.get('/account/bankAccount/exists', queryParameters: {
      "code": code,
    });
    return res.toString();
  }

  Future<String> sendMoney(String s, String r, String amount) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.post('/account/transaction', queryParameters: {
      "sender": s,
      "receiver": r,
      "amount": amount,
    });
    return res.toString();
  }

  Future<String> checkTransaction(String code, String email) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res =
        await api.get('/account/bankAccount/checkReceiver', queryParameters: {
      "code": code,
      "email": email,
    });
    return res.toString();
  }

  Future<void> refreshAccount(int s) async {
    accessToken = await _storage.read(key: 'accessToken');
    await api.get('/account/bankAccount/refreshAccount', queryParameters: {
      "code": s,
    });
  }

  Future<List<Agency>> getAgencies(String bankCode) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get(
      '/adminDash/bank/agencies',
      queryParameters: {
        "bank code":bankCode,
      }
    );
    List<Agency> agencies = [];
    for (var i in res.data) agencies.add(Agency.fromJson(i));
    return agencies;
  }

  Future<List<Bank>> getBanks() async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res = await api.get(
      '/adminDash/bank/all',
    );
    List<Bank> banks = [];
    for (var i in res.data) banks.add(Bank.fromJson(i));
    return banks;
  }

  Future<List<String>> getAccountTypes(String code) async {
    accessToken = await _storage.read(key: 'accessToken');
    Response res =
        await api.get('/adminDash/bank/accountTypes', queryParameters: {
      "bank code": code,
    });
    List<String> types = [];
    for (var i in res.data) types.add(i.toString());
    return types;
  }

  Future<UserResponse?> getUser() async {
    accessToken = await _storage.read(key: 'accessToken');
    UserResponse? user;
    try {
      Response userData = await api.get('/account/profile');
      user = UserResponse(
        firstName: userData.data.toString().substring(
              userData.data.toString().indexOf("firstName: ") + 11,
              userData.data.toString().indexOf(", lastName:"),
            ),
        lastName: userData.data.toString().substring(
              userData.data.toString().indexOf(", lastName:") + 11,
              userData.data.toString().indexOf(", email:"),
            ),
        email: userData.data.toString().substring(
              userData.data.toString().indexOf(", email:") + 9,
              userData.data.toString().indexOf(", phoneNumber:"),
            ),
        phoneNumber: userData.data.toString().substring(
              userData.data.toString().indexOf(", phoneNumber:") + 15,
              userData.data.toString().indexOf(", gender:"),
            ),
        gender: userData.data.toString().substring(
              userData.data.toString().indexOf(", gender:") + 10,
              userData.data.toString().indexOf(", typeOfID:"),
            ),
        typeOfId: userData.data.toString().substring(
              userData.data.toString().indexOf(", typeOfID:") + 12,
              userData.data.toString().indexOf(", idNumber:"),
            ),
        idNumber: userData.data.toString().substring(
              userData.data.toString().indexOf(", idNumber:") + 12,
              userData.data.toString().indexOf(", referralCode:"),
            ),
        referralCode: userData.data.toString().substring(
              userData.data.toString().indexOf(", referralCode:") + 16,
              userData.data.toString().indexOf(", expirationDate:"),
            ),
        expirationDate: userData.data.toString().substring(
              userData.data.toString().indexOf(", expirationDate:") + 18,
              userData.data.toString().indexOf(", birthday:"),
            ),
        BirthDate: userData.data.toString().substring(
              userData.data.toString().indexOf(", birthday:") + 12,
              userData.data.toString().indexOf(", countryOfResidence:"),
            ),
        countryOfResidence: userData.data.toString().substring(
              userData.data.toString().indexOf(", countryOfResidence:") + 22,
              userData.data.toString().indexOf(", address:"),
            ),
        address: userData.data.toString().substring(
              userData.data.toString().indexOf(", address:") + 11,
              userData.data.toString().indexOf("}"),
            ),
      );
    } on DioError catch (e) {
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        print('Error sending request!');
        print(e.message);
      }
    }
    return user;
  }

  Future<String> updateProfile(
      String user, String imagePath, String fileName) async {
    FormData formData = FormData.fromMap(
      {
        'information': user,
        "file": await MultipartFile.fromFile(
          imagePath,
          filename: fileName + ".png",
        ),
      },
    );
    Response res = await api.post(
      '/account/changeProfileInformation',
      data: formData,
      onSendProgress: (sent, total) {
        final progress = (sent / total * 100) ~/ 1;
        print(progress);
      },
    );
    return res.toString();
  }

  Future<String> updateProfile2(String user) async {
    FormData formData = FormData.fromMap(
      {
        'information': user,
      },
    );
    Response res = await api.post(
      '/account/changeProfileInformation2',
      data: formData,
    );
    return res.toString();
  }

  Future<String> changePassword(String password, String oldPassword) async {
    Response res = await api.post('/account/changePassword', queryParameters: {
      "password": password,
      "old password": oldPassword,
    });
    return res.toString();
  }
}
