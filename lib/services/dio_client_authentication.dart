import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/models/user.dart';

class DioClient {
  final _storage = const FlutterSecureStorage();
  final Dio _dio = Dio(
    BaseOptions(
        baseUrl: 'http://10.0.2.2:8082/app',
        validateStatus: (code) {
          if (code! == 403) {
            print('forbidden');
            return false;
          }
          return true;
        }),
  );
  Dio get dio => _dio;


  Future<String> phoneTest({required String phoneNumber}) async {
    Response response = await _dio.get(
      '/reg/test/phone/' + phoneNumber,
    );
    return response.headers.value("taken").toString();
  }
  Future<bool> tokenTest() async {
    final refreshToken = await _storage.read(key: 'refreshToken');
    Response response = await _dio.get(
      '/account/token/check',
      queryParameters: {
        "token": refreshToken,
      },
    );
    return response.data.toString().contains("Session is still active.");
  }

  Future<String> emailTest({required String email}) async {
    Response response = await _dio.get(
      '/reg/test/email/' + email,
    );
    return response.headers.value("taken").toString();
  }

  Future<String> otp(
      {required String phoneNumber, required String appSig}) async {
    print(appSig+"                               aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    FormData formData = FormData.fromMap({
      "phoneNumber": phoneNumber,
      "appSig": appSig,
    });
    Response response = await _dio.post(
      '/phone',
      data: formData,
    );
    return response.headers.value("outcome").toString();
  }

  Future<String?> VerifyOtp(
      {required String phoneNumber, required String code}) async {
    FormData formData = FormData.fromMap({
      "phoneNumber": phoneNumber,
      'code': code,
    });

    Response response = await _dio.post(
      '/phone/verify',
      data: formData,
    );


    await _storage.write(
        key: 'mobileAuth',
        value: "true");

    return (response.headers.value("outcome"));
  }

  Future<String> login(
      {required String login, required String pass}) async {
    FormData formData = FormData.fromMap({
      "username": login,
      'password': pass,
    });
    Response response = await _dio.post(
      '/login',
      data: formData,
    );
    await _storage.write(
        key: 'refreshToken',
        value: response.headers.value("refresh_token").toString());
    await _storage.write(
        key: 'accessToken',
        value: response.headers.value("access_token").toString());
    await _storage.write(
        key: 'mobileAuth',
        value: "false");

    await _storage.write(
        key: 'name',
        value: response.headers.value("name").toString());
    await _storage.write(
        key: 'gender',
        value: response.headers.value("gender").toString());
    await _storage.write(
        key: 'email',
        value: login);


    return response.headers.value("error").toString();
  }

  Future<Map<String, String>> getNumber({required String email}) async {
    Response response2 = await _dio.get(
      '/reg/getNumber',
      options: Options(),
      queryParameters: {
        "email": email,
      },
    );
    await _storage.write(
        key: 'number',
        value: response2.headers.value("number").toString());
    return {
      "number": response2.headers.value("number").toString(),
    };
  }

  Future<void> upload({required String imagePath}) async {
    var bytes = (await rootBundle.load(imagePath)).buffer.asUint8List();

    FormData formData = FormData.fromMap(
        {"file": MultipartFile.fromBytes(bytes, filename: 'photo.jpg')});
    await _dio.post(
      '/account/files',
      data: formData,
      onSendProgress: (sent, total) {
        final progress = (sent / total * 100) ~/ 1;
        print(progress);
      },
    );
  }

  Future<String> createUser(
      {required User userInfo, required String imagePath}) async {
    FormData formData = FormData.fromMap({
      'user': userInfo.data.toJsonString(),
      "file": await MultipartFile.fromFile(
        imagePath,
        filename: userInfo.data.firstName +"_"+ userInfo.data.lastName+".png",
      ),
    });
   Response res= await _dio.post(
      '/reg',
      data: formData,
      onSendProgress: (sent, total) {
        final progress = (sent / total * 100) ~/ 1;
        print(progress);
      },
    );
print(res.toString());
   return res.toString();
  }

}
