import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/screens/authentication/welcome_page.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/bottom_nav.dart';
import 'package:get/get.dart';

import '../../services/dio_client_authentication.dart';


class SplashScreen extends StatefulWidget {
  @override
  Splash createState() => Splash();
}
class Splash extends State<SplashScreen> {
  final _storage = const FlutterSecureStorage();
  @override
  void initState() {
    super.initState();
  }
  final DioClient _client = DioClient();
  var test=false;
  @override
  Widget build(BuildContext context) {
    testToken();
    var duration = const Duration(seconds: 2);
    Timer(duration, () {
      Get.off(() =>test? BottomNav():WelcomePage(),
          transition: Transition.fade,
          duration: Duration(
            milliseconds: 1000,
          ));
    });

    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: new BoxDecoration(color: Styles.bgColor(context)),
          child: new Center(
            child: Image(image: Styles.splashImage(context), height: 300),
          ),
        ),
      ),
    );
  }
  Future<void> testToken()  async {
    bool test2= await _client.tokenTest();
    String? auth= await _storage.read(
      key: 'mobileAuth',);

    test=test2 && (auth!.compareTo("true")==0);
  }
}
