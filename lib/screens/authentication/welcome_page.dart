import 'package:flutter/material.dart';
import 'package:frontend/screens/authentication/login_page.dart';
import 'package:frontend/screens/authentication/otp/phone_input.dart';
import 'package:get/get.dart';
import '../../utils/styles.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Styles.bgColor(context),
      body: SafeArea(
        child: CustomScrollView(
          reverse: true,
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 70, vertical: 50),
                child: Column(
                  children: [
                    Flexible(
                      child: Column(
                        children: [
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Image(
                                image: AssetImage(
                                    'assets/images/team_illustration.png'),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 95,
                          ),
                          Text(
                            'Welcome',
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.headline5!.copyWith(
                                      color: Styles.titleColor(context),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 35,
                                    ),
                          ),
                          const SizedBox(
                            height: 13,
                          ),
                          Text(
                            'Hope you can find use of our app.',
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.subtitle1!.copyWith(
                                      color: Styles.subTextColor(context),
                                      letterSpacing: 0.8,
                                      fontSize: 20,
                                      fontFamily: 'Akshar',
                                    ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 7),
                      height:60,
                      decoration: BoxDecoration(
                        color: Styles.AccentColor(context),
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 0),
                            blurRadius: 0.2,
                            spreadRadius: 0.1,
                            color: Colors.black26,
                          ),
                        ],
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 0),
                                    blurRadius: 0.8,
                                    spreadRadius: 0.8,
                                    color: Colors.black26,
                                  ),
                                ],
                                color: Styles.headerColor(context),
                                borderRadius: BorderRadius.circular(100),
                              ),
                              child: TextButton(
                                onPressed: () {
                                  Get.to(()=>
                                    LoginPage(),
                                    transition: Transition.rightToLeftWithFade,
                                    duration: Duration(milliseconds: 200),
                                  );
                                },
                                child: Text(
                                  'Sign In',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: TextButton(
                              onPressed: () {
                                Get.to(()=>
                                  PhoneInput(),
                                  transition: Transition.rightToLeftWithFade,
                                  duration: Duration(milliseconds: 200),
                                );
                              },
                              child: Text(
                                'Sign Up',
                                style: TextStyle(
                                  color: Styles.titleColor(context),
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
