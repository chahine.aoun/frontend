import 'package:flutter/material.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';

import '../../models/banks_and_agencies/agency.dart';
import '../../models/banks_and_agencies/bank.dart';
import '../../services/api_dio.dart';
import '../../widgets/bottom_nav.dart';
import '../../widgets/my_button.dart';

class CreateBankAccount extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreateBankAccount();
}

class _CreateBankAccount extends State<CreateBankAccount> {
  String hint1 = "Pick an agency*";
  String hint2 = "Pick a bank*";
  String hint3 = "Pick an account type*";
  List<Agency> agencies = [];
  List<String> types = [];
  bool aaa = false;
  bool bbb = false;
  bool valid = false;
  final Api api = Api();
  String type = "";
  String swiftCode = "";
  String agencyCode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      bottomSheet: Container(
        color: Styles.bgColor(context),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: elevatedButton(
          width: double.infinity,
          color: Styles.selectedItemColor(context),
          context: context,
          callback: valid
              ? () {
                  createAccount(type, swiftCode, agencyCode);
                  Get.offAll(
                    () => BottomNav(),
                    transition: Transition.downToUp,
                    duration: Duration(
                      milliseconds: 250,
                    ),
                  );
                }
              : null,
          text: 'Create account',
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      backgroundColor: Styles.bgColor(context),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Text(
                "Account Creation",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      color: Styles.titleColor(context),
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Select the type of bank account you want to create.',
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: Styles.subTextColor(context).withOpacity(0.9),
                      letterSpacing: 0.8,
                      fontSize: 20,
                      fontFamily: 'Akshar',
                    ),
              ),
            ],
          ),
          FutureBuilder<List<Bank>>(
            future: loadBanks(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Bank>> snapshot) {
              if (snapshot.hasData) {
                return Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Styles.bgColor(context),
                          borderRadius: BorderRadius.circular(18),
                          border: Border.all(
                            color: Styles.fieldColor(context),
                          ),
                        ),
                        child: new DropdownButton<String>(
                          hint: Text(
                            hint2,
                            style: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 20,
                            ),
                          ),
                          borderRadius: BorderRadius.circular(20),
                          dropdownColor: Styles.AccentColor(context),
                          underline: Container(),
                          icon: Icon(
                            Icons.arrow_drop_down_rounded,
                            color: Styles.whiteAndBlack(context),
                            size: 40,
                          ),
                          items: snapshot.data!
                              .map<DropdownMenuItem<String>>((Bank value) {
                            return DropdownMenuItem<String>(
                              value: value.name,
                              child: Text(value.name),
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            setState(
                                  () {
                                    aaa = true;
                                    hint2 = value!;
                                    snapshot.data!.forEach(
                                      (element) async {
                                    if (element.name == value) {
                                      types = await loadAccountTypes(
                                          element.swiftCode);
                                      await loadAgencies(element.swiftCode);
                                      bbb = true;
                                      swiftCode = element.swiftCode;
                                      hint3 = "Pick an account type*";
                                    }
                                    valid = false;
                                  },
                                );
                              },
                            );
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Styles.bgColor(context),
                          borderRadius: BorderRadius.circular(18),
                          border: Border.all(
                            color: Styles.fieldColor(context),
                          ),
                        ),
                        child: new DropdownButton<String>(
                          hint: Text(
                            hint1,
                            style: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 20,
                            ),
                          ),
                          borderRadius: BorderRadius.circular(20),
                          dropdownColor: Styles.AccentColor(context),
                          underline: Container(),
                          icon: Icon(
                            Icons.arrow_drop_down_rounded,
                            color: Styles.whiteAndBlack(context),
                            size: 40,
                          ),
                          items: agencies.map<DropdownMenuItem<String>>((Agency value) {
                            return DropdownMenuItem<String>(
                              value: value.name,
                              child: Text(value.name),
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            agencies.forEach((element) {
                              if(element.name.compareTo(value!)==0){
                              setState(
                                    () {
                                  hint1 = value;
                                  agencyCode=element.code;
                                },
                              );
                              }
                            },);
                          },
                        ),
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Styles.bgColor(context),
                          borderRadius: BorderRadius.circular(18),
                          border: Border.all(
                            color: Styles.fieldColor(context),
                          ),
                        ),
                        child: new DropdownButton<String>(
                          hint: Text(
                            hint3,
                            style: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 20,
                            ),
                          ),
                          borderRadius: BorderRadius.circular(20),
                          dropdownColor: Styles.AccentColor(context),
                          underline: Container(),
                          icon: Icon(
                            Icons.arrow_drop_down_rounded,
                            color: Styles.whiteAndBlack(context),
                            size: 40,
                          ),
                          items: bbb
                              ? types.map<DropdownMenuItem<String>>(
                                  (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList()
                              : const [
                                  DropdownMenuItem<String>(
                                      value: "Pick an account type*",
                                      child: Text('Pick a bank first.')),
                                ],
                          onChanged: (String? value) {
                            setState(
                              () {
                                hint3 = value!;
                                type = value;
                                valid = true;
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container(
                child: Center(child: CircularProgressIndicator()),
                color: Styles.bgColor(context),
              );
            },
          ),
        ],
      ),
    );
  }

  Future<List<Agency>> loadAgencies(String bankCode) async {
    var m = await api.getAgencies(bankCode);
    setState(() {
      agencies=m;
    });
    return m;
  }

  Future<List<Bank>> loadBanks() async {
    var m = await api.getBanks();
    return m;
  }

  Future<List<String>> loadAccountTypes(String code) async {
    var m = await api.getAccountTypes(code);
    return m;
  }

  Future<void> createAccount(
      String type, String swiftCode, String agencyCode) async {
    await api.createBankAccount(type, swiftCode, agencyCode);
  }
}
