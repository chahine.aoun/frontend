import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontend/controller/otp_controller.dart';
import 'package:frontend/screens/authentication/create_bank_account.dart';
import 'package:frontend/screens/authentication/terms.dart';
import 'package:frontend/services/api_dio.dart';
import 'package:frontend/services/dio_client_authentication.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/bottom_nav.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:reactive_pinput/reactive_pinput.dart';

import 'package:sms_autofill/sms_autofill.dart';

class OtpView extends GetView<OtpController> with CodeAutoFill {
  OtpView({Key? key}) : super(key: key);
  Api _client2 = Api();
  @override
  void codeUpdated() {
    controller.otpForm.updateValue({controller.otpControl: '$code'});
  }

  void listenOtp() async {
    await SmsAutoFill().unregisterListener();
    listenForCode();
    await SmsAutoFill().listenForCode;
  }

  String number = "";
  final DioClient _client = DioClient();
  final Api api = Api();

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => OtpController());
    listenOtp();
    if (Get.previousRoute == '/PhoneInput' ||
        Get.previousRoute == '/LoginPage' ||
        Get.previousRoute == '/Edit') {
      number = Get.arguments['phone'];
    }

    controller.number = number;
    return ReactiveForm(
      formGroup: controller.otpForm,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Styles.bgColor(context),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.keyboard_backspace_rounded,
              size: 30.0,
              color: Styles.whiteAndBlack(context),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              child: SvgPicture.asset(
                'assets/images/top_right_large.svg',
                width: Get.width * 0.4,
                height: Get.height * 0.4,
                color: Styles.headerColor(context),
              ),
            ),
            SingleChildScrollView(
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
                top: Get.height * 0.2,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //  Screen title: Verification
                  Center(
                    child: Text(
                      "Verification",
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Styles.titleColor(context),
                          ),
                    ),
                  ),
                  const SizedBox(height: 25.0),
                  Center(
                    child: Column(
                      children: [
                        Text(
                          "Please enter the 6 digit verification code sent to ",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontFamily: 'Akshar',
                                    fontSize: 17,
                                    letterSpacing: 1,
                                    fontWeight: FontWeight.bold,
                                    height: 1.4,
                                    color: Styles.subTextColor(context),
                                  ),
                        ),
                        Text(
                          number,
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.bold,
                                    height: 1.4,
                                    color: Styles.subTextColor(context),
                                  ),
                        ),
                        const SizedBox(height: 25.0),
                        GestureDetector(
                          onTap: () {
                            Get.back();
                          },
                          child: Text(
                            "Edit Number",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      color: Styles.linkColor(context),
                                    ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 35),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 0),
                          blurRadius: 0.01,
                          spreadRadius: 0.01,
                          color: Colors.black26,
                        ),
                      ],
                      color: Styles.AccentColor(context),
                      borderRadius: BorderRadius.circular(18),
                    ),
                    child: Column(
                      children: [
                        ReactivePinPut<String>(
                          showErrors: (control) =>
                              control.invalid &&
                              control.touched &&
                              control.dirty,
                          onCompleted: (String) => loadingDialog(context),
                          textStyle: TextStyle(
                            color: Styles.whiteAndBlack(context),
                          ),
                          formControlName: controller.otpControl,
                          length: 6,
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(6),
                          ],
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          pinAnimationType: PinAnimationType.fade,
                          submittedPinTheme: PinTheme(
                              height: 30,
                              decoration: _pinPutDecoration(
                                Styles.fieldColor(context),
                              )),
                          focusedPinTheme: PinTheme(
                              height: 30,
                              decoration: _pinPutDecoration(
                                Styles.selectedItemColor(context),
                              )),
                          followingPinTheme: PinTheme(
                              height: 30,
                              decoration: _pinPutDecoration(
                                Styles.fieldColor(context),
                              )),
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty",
                            ValidationMessage.maxLength:
                                "OTP code length should be equal to 6",
                            ValidationMessage.minLength:
                                "OTP code length should be equal to 6",
                          },
                        ),
                        const SizedBox(height: 10.0),
                        Divider(
                          color: Styles.dividerColor(context),
                          thickness: 0.8,
                        ),
                        const SizedBox(height: 15.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Didn't receive code?",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  ?.copyWith(
                                      color: Styles.subTextColor(context)),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            _resendAgainTimerButton(context),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: ReactiveFormConsumer(
          builder: (context, form, child) {
            return ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Styles.selectedItemColor(context),
                onSurface: Styles.activeColor(context),
                shape: const CircleBorder(),
                padding: const EdgeInsets.all(20.0),
              ),
              onPressed: form.valid
                  ? () async {
                      FocusScopeNode currentFocus = FocusScope.of(context);
                      if (!currentFocus.hasPrimaryFocus) {
                        currentFocus.unfocus();
                      }
                      form.updateValueAndValidity();

                      if (Get.arguments['user'] != null) {
                        if (Get.arguments['image'] != null) {
                          print(
                            await updateUserProfile(
                              Get.arguments["user"],
                              Get.arguments['image'],
                              Get.arguments['fileName'],
                            ),
                          );
                        } else {
                          print(
                            await updateUserProfile2(
                              Get.arguments["user"],
                            ),
                          );
                        }
                      }

                      if (Get.arguments['purpose']
                          .toString()
                          .contains('registration')) {
                        Get.to(
                          () => Terms(),
                          transition: Transition.downToUp,
                          duration: Duration(
                            milliseconds: 400,
                          ),
                          arguments: {'phone': number},
                        );
                      } else if (await _client2.count() == 0) {
                        Get.off(
                          () => CreateBankAccount(),
                          transition: Transition.rightToLeftWithFade,
                          duration: Duration(
                            milliseconds: 400,
                          ),
                        );
                      } else {
                        Get.offAll(
                          () => BottomNav(),
                          transition: Transition.rightToLeftWithFade,
                          duration: Duration(
                            milliseconds: 400,
                          ),
                        );
                      }
                    }
                  : null,
              child: Icon(
                Icons.arrow_forward_ios_rounded,
                size: 25.0,
                color: Colors.white,
              ),
            );
          },
        ),
      ),
    );
  }

  BoxDecoration _pinPutDecoration(Color color) {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(12),
      border: Border.all(
        width: 1.5,
        color: color,
      ),
    );
  }

  ArgonTimerButton _resendAgainTimerButton(BuildContext context) {
    return ArgonTimerButton(
      height: 16.0,
      width: 95.0,
      minWidth: 30,
      initialTimer: 25,
      highlightColor: Colors.transparent,
      highlightElevation: 0,
      color: Colors.transparent,
      elevation: 0,
      child: Text(
        "Request again",
        style: Theme.of(context).textTheme.bodyText2!.copyWith(
              fontWeight: FontWeight.bold,
              color: Styles.linkColor(context),
            ),
      ),
      loader: (timeLeft) {
        return Text(
          "$timeLeft",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontWeight: FontWeight.bold,
                color: Styles.subTextColor(context),
              ),
        );
      },
      onTap: (startTimer, btnState) async {
        var appSignatureID = await SmsAutoFill().getAppSignature;
        if (btnState == ButtonState.Idle) {
          startTimer(30);
          _client.otp(phoneNumber: number, appSig: appSignatureID);
        }
      },
    );
  }

  void loadingDialog(BuildContext context) async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return Dialog(
            backgroundColor: Styles.AccentColor(context),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    color: Styles.linkColor(context),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Verifying...',
                    style: TextStyle(color: Styles.whiteAndBlack(context)),
                  )
                ],
              ),
            ),
          );
        });
    await Future.delayed(const Duration(seconds: 3));
    Navigator.of(context).pop();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          controller.otpMessage,
          style: TextStyle(color: Colors.white),
        ),
        elevation: 1.0,
        backgroundColor: const Color(0xFF024751),
        duration: Duration(seconds: 3),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
      ),
    );
  }

  Future<String> updateUserProfile(
      String user, String imagePath, String fileName) async {
    return await api.updateProfile(user, imagePath, fileName);
  }

  Future<String> updateUserProfile2(String user) async {
    return await api.updateProfile2(user);
  }
}
