import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontend/services/dio_client_authentication.dart';
import 'package:frontend/screens/authentication/otp/otp_view.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/country_picker_dialog.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:sms_autofill/sms_autofill.dart';

class PhoneInput extends StatefulWidget {
  @override
  _PhoneInputState createState() => _PhoneInputState();
}

class _PhoneInputState extends State<PhoneInput> {
  final DioClient _client = DioClient();
  GlobalKey<FormState> _formKey = GlobalKey();
  bool isValid = false;
  String number = "1";

  @override
  Widget build(BuildContext context) {
    return Form(
      onChanged: () {
        setState(() {
          isValid = _formKey.currentState!.validate();
        });
      },
      autovalidateMode: AutovalidateMode.always,
      key: _formKey,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Styles.bgColor(context),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.keyboard_backspace_rounded,
              size: 30.0,
              color: Styles.whiteAndBlack(context),
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              child: SvgPicture.asset(
                'assets/images/top_right_large.svg',
                width: Get.width * 0.4,
                height: Get.height * 0.4,
                color: Styles.headerColor(context),
              ),
            ),
            SingleChildScrollView(
              padding: EdgeInsets.only(
                left: 20.0,
                right: 20.0,
                top: Get.height * 0.2,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //  Screen title: Verification
                  Center(
                    child: Text(
                      "Verification",
                      style: Theme.of(context).textTheme.headline5!.copyWith(
                            fontWeight: FontWeight.bold,
                            color: Styles.titleColor(context),
                          ),
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Please enter the phone number you would like to link to your account.",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontFamily: 'Akshar',
                            fontSize: 17,
                            letterSpacing: 1,
                            fontWeight: FontWeight.bold,
                            height: 1.4,
                            color: Styles.subTextColor(context),
                          ),
                    ),
                  ),
                  const SizedBox(height: 50),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 0),
                          blurRadius: 0.01,
                          spreadRadius: 0.01,
                          color: Colors.black26,
                        ),
                      ],
                      color: Styles.AccentColor(context),
                      borderRadius: BorderRadius.circular(18),
                    ),
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 5),
                      child: Column(
                        children: [
                          IntlPhoneField(
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                            ],
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                color: Styles.subTextColor(context),
                                fontSize: 15,
                              ),
                              counterStyle: TextStyle(
                                color: Styles.textColor(context),
                              ),
                              contentPadding: EdgeInsets.all(20),
                              hintText: "Phone Number",
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.fieldColor(context),
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedErrorBorder:  OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.selectedItemColor(context),
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            dropdownIcon: Icon(
                              Icons.arrow_drop_down_rounded,
                              size: 35,
                              color: Styles.iconColor(context),
                            ),
                            dropdownTextStyle: TextStyle(
                              color: Styles.textColor(context),
                            ),
                            pickerDialogStyle: PickerDialogStyle(
                              searchFieldCursorColor: Styles.textColor(context),
                              searchFieldInputDecoration: InputDecoration(
                                hintText: "Search",
                                suffixIcon: Icon(
                                  Icons.search_rounded,
                                  color: Styles.iconColor(context),
                                  size: 30,
                                ),
                                hintStyle: TextStyle(
                                  color: Styles.subTextColor(context),
                                  fontSize: 15,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Styles.fieldColor(context),
                                    width: 1.5,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Styles.selectedItemColor(context),
                                    width: 1.8,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                    width: 1.5,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                    width: 1.8,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              listTileDivider: Divider(
                                color: Styles.fieldColor(context),
                                thickness: 0.4,
                              ),
                              backgroundColor: Styles.AccentColor(context),
                              countryCodeStyle:
                                  TextStyle(color: Styles.textColor(context)),
                              countryNameStyle:
                                  TextStyle(color: Styles.textColor(context)),
                            ),
                            keyboardAppearance: Styles.getTheme(context)
                                ? Brightness.dark
                                : Brightness.light,
                            disableLengthCheck: false,
                            invalidNumberMessage:
                                "Please make sure you entered a valid number.",
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                            cursorColor: Styles.textColor(context),
                            initialCountryCode: 'TN',
                            onChanged: (phone) {
                              number = phone.completeNumber;
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Styles.selectedItemColor(context),
            onSurface: Styles.activeColor(context),
            shape: const CircleBorder(),
            padding: const EdgeInsets.all(20.0),
          ),
          onPressed: isValid
              ? () async {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  String outcome = await validCode();
                  loadingDialog(context, outcome);
                }
              : null,
          child: Icon(
            Icons.arrow_forward_ios_rounded,
            size: 25.0,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Future<String> validCode() async {
    String test = await _client.phoneTest(phoneNumber: number);

    if (test.compareTo("false")==0) {
      var appSignatureID = await SmsAutoFill().getAppSignature;
      print(appSignatureID+"                               here");
      String outcome =
          await _client.otp(phoneNumber: number, appSig: appSignatureID);
      return outcome;
    } else {
      return "This number was used to create an existing account.";
    }
  }

  void loadingDialog(BuildContext context, String message) async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return Dialog(
            backgroundColor: Styles.AccentColor(context),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    color: Styles.linkColor(context),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Verifying...',
                    style: TextStyle(color: Styles.whiteAndBlack(context)),
                  )
                ],
              ),
            ),
          );
        });
    await Future.delayed(const Duration(seconds: 2));
    Navigator.of(context).pop();
    if (message.compareTo("The code was sent to your number.")==0) {
      Get.to(
            () => OtpView(),
        transition: Transition.rightToLeft,
        duration: Duration(
          milliseconds: 300,
        ),
        arguments: {
          'phone': number,
          'purpose': 'registration',
        },
      );
    }else{
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: TextStyle(color: Colors.white),
        ),
        elevation: 1.0,
        backgroundColor: const Color(0xFF024751),
        duration: Duration(seconds: 2),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
      ),
    );
}
  }
}
