import 'package:flutter/material.dart';
import 'package:frontend/screens/authentication/registration/registration_form.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';

class Terms extends StatefulWidget {
  const Terms({Key? key}) : super(key: key);

  @override
  State<Terms> createState() => _TermsState();
}

class _TermsState extends State<Terms> {
  String number = "";
  bool value = false;
  bool doneReading = false;
  final controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    if (Get.previousRoute == '/OtpView') {
      number = Get.arguments['phone'];
    }
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.offset) {
        doneReading = true;
        setState(() {});
      }
    });
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Styles.bgColor(context),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: 30,
                ),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 0),
                      blurRadius: 0.01,
                      spreadRadius: 0.01,
                      color: Colors.black26,
                    ),
                  ],
                  color: Styles.AccentColor(context),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Center(
                      child: Text(
                        "Read the full text to continue.",
                        style: TextStyle(
                          color: Styles.whiteAndBlack(context),
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 20,
                      ),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 15.0,
                          vertical: 15.0,
                        ),
                        height: MediaQuery.of(context).size.height * 0.25,
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            new Expanded(
                              flex: 1,
                              child: new SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                controller: controller,
                                child: new Text(
                                  "1 Description that is too long in text format(Here Data is coming from API) jdlksaf j klkjjflkdsjfkddfdfsdfds " +
                                      "2 Description that is too long in text format(Here Data is coming from API) d fsdfdsfsdfd dfdsfdsf sdfdsfsd d " +
                                      "3 Description that is too long in text format(Here Data is coming from API)  adfsfdsfdfsdfdsf   dsf dfd fds fs" +
                                      "4 Description that is too long in text format(Here Data is coming from API) dsaf dsafdfdfsd dfdsfsda fdas dsad" +
                                      "5 Description that is too long in text format(Here Data is coming from API) dsfdsfd fdsfds fds fdsf dsfds fds " +
                                      "6 Description that is too long in text format(Here Data is coming from API) asdfsdfdsf fsdf sdfsdfdsf sd dfdsf" +
                                      "7 Description that is too long in text format(Here Data is coming from API) df dsfdsfdsfdsfds df dsfds fds fsd" +
                                      "8 Description that is too long in text format(Here Data is coming from API)" +
                                      "9 Description that is too long in text format(Here Data is coming from API)" +
                                      "10 Description that is too long in text format(Here Data is coming from API)",
                                  style: new TextStyle(
                                    fontSize: 16.0,
                                    color: Styles.whiteAndBlack(context),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 0.3,
                            color: Styles.dividerColor(context),
                          ),
                          color:
                              Styles.accentColor2(context).withOpacity(0.015),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Checkbox(
                        value: this.value,
                        activeColor: Styles.accentColor2(context),
                        shape: CircleBorder(),
                        checkColor: Styles.selectedItemColor(context),
                        onChanged: (bool? value) {
                          setState(() {
                            this.value = value!;
                          });
                        },
                      ),
                      title: Text(
                        "I accept the terms and conditions to use this service.",
                        style: TextStyle(color: Styles.whiteAndBlack(context)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Styles.selectedItemColor(context),
          onSurface: Styles.activeColor(context),
          shape: const CircleBorder(),
          padding: const EdgeInsets.all(20.0),
        ),
        onPressed: value & doneReading
            ? () {
                Get.to(
                  () => RegistrationForm(),
                  transition: Transition.downToUp,
                  duration: Duration(
                    milliseconds: 400,
                  ),
                  arguments: {'phone': number},
                );
              }
            : null,
        child: Icon(
          Icons.arrow_forward_ios_rounded,
          size: 25.0,
          color: Colors.white,
        ),
      ),
    );
  }
}
