import 'dart:async';

import 'package:flutter/material.dart';
import 'package:frontend/screens/authentication/splash_screen.dart';
import 'package:frontend/utils/view_model.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class Useless extends StatelessWidget {
  const Useless({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ViewModel vm = context.watch<ViewModel>();
    vm.getPref(context);
    var duration =  Duration(microseconds: 10);
    Timer(duration, () {
      Get.off(() => SplashScreen(),
        transition: Transition.fade
      );
    });
    return Scaffold();
  }
}

