import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/models/data/data.dart';
import 'package:frontend/models/user.dart';
import 'package:frontend/screens/authentication/login_page.dart';
import 'package:frontend/services/dio_client_authentication.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:reactive_image_picker/image_file.dart';
import 'package:reactive_image_picker/reactive_image_picker.dart';

class FourthForm extends GetView<FieldController> {
  FourthForm({
    Key? key,
    required this.onStepCancel,
    required this.number,
  }) : super(key: key);
  final VoidCallback onStepCancel;
  final String number;

  @override
  Widget build(BuildContext context) {
    Get.find<FieldController>();
    final DioClient _client = DioClient();
    return ReactiveForm(
      formGroup: controller.fieldFormGroup4,
      child: Column(
        children: [
          SizedBox(
            height: 5.0,
          ),
          ReactiveDropdownField(
            validationMessages: (control) => {
              ValidationMessage.required: "Please pick you ID card's type.",
            },
            borderRadius: BorderRadius.circular(18),
            iconEnabledColor: Styles.whiteAndBlack(context),
            dropdownColor: Styles.AccentColor(context),
            formControlName: 'dropdown',
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.perm_identity_rounded,
                size: 15,
                color: Styles.iconColor(context),
              ),
              contentPadding: EdgeInsets.all(20),
              labelText: 'ID Type *',
              labelStyle: TextStyle(
                color: Styles.textColor(context),
                fontSize: 15,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Styles.fieldColor(context),
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Styles.selectedItemColor(context),
                  width: 1.8,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 1.8,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
            ),
            items: const [
              DropdownMenuItem<bool>(value: true, child: Text('Passport')),
              DropdownMenuItem<bool>(value: false, child: Text('ID card')),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: MyTextField(
              inputFormatter: [
                FilteringTextInputFormatter.digitsOnly,
              ],
              context: context,
              hintText: 'ID Number',
              labelTxt: "ID Number *",
              icon: Icons.numbers_rounded,
              inputType: TextInputType.number,
              controlName: "idNumber",
              validationMessages: (control) => {
                ValidationMessage.required: "This field must not be empty.",
                ValidationMessage.number: 'Please enter a valid ID number.',
                ValidationMessage.minLength: 'Please enter a valid ID number.',
              },
            ),
          ),
          Theme(
            data: Theme.of(context).copyWith(
              colorScheme: ColorScheme.light(
                primary: Styles.AccentColor(context), // header background color
                onPrimary: Styles.whiteAndBlack(context), // header text color
                onSurface: Styles.whiteAndBlack(context), // body text color
              ),
              dividerColor: Styles.dividerColor(context),
              dialogTheme: DialogTheme(
                backgroundColor: Styles.bgColor(context),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              textButtonTheme: TextButtonThemeData(
                style: TextButton.styleFrom(
                  primary: Styles.whiteAndBlack(context), // button text color
                ),
              ),
            ),
            child: ReactiveDatePicker<DateTime>(
              formControlName: 'date',
              firstDate: DateTime.now(),
              lastDate: DateTime.utc(DateTime.now().year + 5),
              initialDatePickerMode: DatePickerMode.year,
              builder: (context, picker, child) {
                return ReactiveTextField(
                  formControlName: 'date',
                  valueAccessor: DateTimeValueAccessor(
                    dateTimeFormat: DateFormat('dd MMM yyyy'),
                  ),
                  validationMessages: (control) => {
                    ValidationMessage.required: "This field must not be empty.",
                  },
                  onTap: () {
                    picker.showPicker();
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                    prefixIcon: Icon(
                      Icons.calendar_month_rounded,
                      size: 15,
                      color: Styles.iconColor(context),
                    ),
                    contentPadding: EdgeInsets.all(20),
                    labelText: "ID's expiration date *",
                    hintText: "DD-MM-YYYY",
                    labelStyle: TextStyle(
                      color: Styles.textColor(context),
                      fontSize: 15,
                    ),
                    hintStyle: TextStyle(
                      color: Styles.subTextColor(context),
                      fontSize: 15,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Styles.fieldColor(context),
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(18),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Styles.selectedItemColor(context),
                        width: 1.8,
                      ),
                      borderRadius: BorderRadius.circular(18),
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                        width: 1.8,
                      ),
                      borderRadius: BorderRadius.circular(18),
                    ),
                    errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.red,
                        width: 1.5,
                      ),
                      borderRadius: BorderRadius.circular(18),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          ReactiveImagePicker(
            formControlName: "image",
            decoration: InputDecoration(
              labelText: "Image of the ID piece *",
              labelStyle: TextStyle(
                color: Styles.textColor(context),
                fontSize: 15,
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Styles.fieldColor(context),
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Styles.selectedItemColor(context),
                  width: 1.8,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 1.8,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.red,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(18),
              ),
            ),
            validationMessages: (control) => {
              ValidationMessage.required: "This field must not be empty.",
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: MyTextField(
              inputFormatter: [
                FilteringTextInputFormatter.digitsOnly,
              ],
              context: context,
              hintText: 'Referral code',
              labelTxt: "Referral code ",
              icon: Icons.numbers_rounded,
              inputType: TextInputType.number,
              controlName: "referral",
              validationMessages: (control) => {},
            ),
          ),
          ReactiveFormConsumer(
            builder: (buildContext, form, child) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: form.valid
                          ? () async {
                              ImageFile image = controller
                                  .fieldFormGroup4.value["image"] as ImageFile;
                              String s = await _client.createUser(
                                  userInfo: createUser(),
                                  imagePath: image.image
                                      .toString()
                                      .substring(8)
                                      .replaceAll("'", ""));
                              loadingDialog(context);
                              outcome(context, s);
                              if (s.contains("Account created successfully")) {
                                Get.offAll(
                                  () => LoginPage(),
                                  transition: Transition.fade,
                                  duration: Duration(
                                    milliseconds: 400,
                                  ),
                                );
                              }
                            }
                          : null,
                      icon: Icon(
                        Icons.done_rounded,
                        size: 30.0,
                        color: form.valid
                            ? Colors.blue
                            : Styles.iconColor(context),
                      )),
                  SizedBox(
                    width: 50,
                  ),
                  IconButton(
                      onPressed: onStepCancel,
                      icon: Icon(
                        Icons.close_rounded,
                        size: 30.0,
                        color: Colors.blue,
                      )),
                ],
              );
            },
          ),
        ],
      ),
    );
  }

  User createUser() {
    Data data = Data(
        email: controller.fieldFormGroup2.value["email"].toString(),
        firstName: controller.fieldFormGroup.value["name"].toString(),
        lastName: controller.fieldFormGroup.value["lastName"].toString(),
        password: controller.fieldFormGroup2.value["password"].toString(),
        phoneNumber: number,
        typeOfId: controller.fieldFormGroup4.value["dropdown"] == true
            ? 'Passport'
            : 'ID card',
        idNumber: controller.fieldFormGroup4.value["idNumber"].toString(),
        expirationDate: controller.fieldFormGroup4.value["date"]
            .toString()
            .substring(0, 10),
        BirthDate: controller.fieldFormGroup3.value["date"]
            .toString()
            .substring(0, 10),
        gender: controller.fieldFormGroup.value["gender"].toString(),
        countryOfResidence:
            controller.fieldFormGroup3.value["countryOfResidence"].toString(),
        address: controller.fieldFormGroup3.value["address"].toString(),
        referralCode:
            controller.fieldFormGroup4.value["referral"].toString() + " ");
    return User(data: data);
  }

  void outcome(BuildContext context, String message) async {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
        elevation: 1.0,
        backgroundColor: const Color(0xFF024751),
        duration: Duration(seconds: 3),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
      ),
    );
  }

  void loadingDialog(BuildContext context) async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return Dialog(
            backgroundColor: Styles.AccentColor(context),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    color: Styles.linkColor(context),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Registering...',
                    style: TextStyle(color: Styles.whiteAndBlack(context)),
                  )
                ],
              ),
            ),
          );
        });
    await Future.delayed(const Duration(seconds: 4));
    Navigator.of(context).pop();
  }
}
