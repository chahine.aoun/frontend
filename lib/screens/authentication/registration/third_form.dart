import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:intl_phone_field/country_picker_dialog.dart';

class ThirdForm extends GetView<FieldController> {
  final TextEditingController textController = TextEditingController(
    text: "Country of Residence *",
  );
  ThirdForm({
    Key? key,
    required this.onStepContinue,
    required this.onStepCancel,
  }) : super(key: key);
  final VoidCallback onStepContinue;
  final VoidCallback onStepCancel;

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => FieldController());
    return ReactiveForm(
      formGroup: controller.fieldFormGroup3,
      child: Column(
        children: [
          SizedBox(
            height: 5.0,
          ),
      Theme(
        data: Theme.of(context).copyWith(
          colorScheme: ColorScheme.light(
            primary: Styles.AccentColor(context), // header background color
            onPrimary: Styles.whiteAndBlack(context), // header text color
            onSurface: Styles.whiteAndBlack(context), // body text color
          ),
          dividerColor: Styles.dividerColor(context),
          dialogTheme: DialogTheme(
            backgroundColor: Styles.bgColor(context),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),),
          ),
          textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              primary: Styles.whiteAndBlack(context), // button text color
            ),
          ),
        ),
          child: ReactiveDatePicker<DateTime>(
            formControlName: 'date',
            firstDate: DateTime(1970),
            lastDate: DateTime.now(),
            initialDatePickerMode: DatePickerMode.year,
            builder: (context, picker, child) {
              return ReactiveTextField(
                formControlName: 'date',
                valueAccessor: DateTimeValueAccessor(
                  dateTimeFormat: DateFormat('dd MMM yyyy'),
                ),
                validationMessages: (control) => {
                  ValidationMessage.required: "This field must not be empty.",
                },
                onTap: () {
                  picker.showPicker();
                },
                readOnly: true,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.calendar_month_rounded,
                    size: 15,
                    color: Styles.iconColor(context),
                  ),
                  contentPadding: EdgeInsets.all(20),
                  labelText: 'Birth date *',
                  hintText: "DD-MM-YYYY",
                  labelStyle: TextStyle(
                    color: Styles.textColor(context),
                    fontSize: 15,
                  ),
                  hintStyle: TextStyle(
                    color: Styles.subTextColor(context),
                    fontSize: 15,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Styles.fieldColor(context),
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Styles.selectedItemColor(context),
                      width: 1.8,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                      width: 1.8,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                      width: 1.5,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                ),
              );
            },
          ),
      ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: IntlPhoneField(
              dropdownIconPosition: IconPosition.trailing,
              dropdownIcon: const Icon(
                Icons.arrow_drop_down_rounded,
                size: 35,
              ),
              showCountryFlag: false,
              controller: textController,
              showCursor: false,
              readOnly: true,
              onCountryChanged: (country) {
                controller.fieldFormGroup3.updateValue({
                  "countryOfResidence": country.name,
                  "date": controller.fieldFormGroup3.value["date"],
                  "address":
                  controller.fieldFormGroup3.value["address"],
                });
                textController.value = TextEditingValue(
                  text: country.name,
                );
              },
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.flag_rounded,
                  size: 15,
                  color: Styles.iconColor(context),
                ),
                labelText: 'Country Of Residence',
                labelStyle: TextStyle(
                  color: Styles.textColor(context),
                  fontSize: 15,
                ),
                counterStyle: TextStyle(
                  color: Styles.textColor(context),
                ),
                contentPadding: EdgeInsets.all(20),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Styles.fieldColor(context),
                    width: 1.5,
                  ),
                  borderRadius: BorderRadius.circular(18),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Styles.fieldColor(context),
                    width: 1.5,
                  ),
                  borderRadius: BorderRadius.circular(18),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Styles.selectedItemColor(context),
                    width: 1.8,
                  ),
                  borderRadius: BorderRadius.circular(18),
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 1.8,
                  ),
                  borderRadius: BorderRadius.circular(18),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.red,
                    width: 1.5,
                  ),
                  borderRadius: BorderRadius.circular(18),
                ),
              ),
              dropdownTextStyle: TextStyle(
                color: Colors.transparent,
                fontSize: 0.5,
              ),
              pickerDialogStyle: PickerDialogStyle(
                searchFieldCursorColor: Styles.textColor(context),
                searchFieldInputDecoration: InputDecoration(
                  hintText: "Search",
                  suffixIcon: Icon(
                    Icons.search_rounded,
                    color: Styles.iconColor(context),
                    size: 30,
                  ),
                  hintStyle: TextStyle(
                    color: Styles.subTextColor(context),
                    fontSize: 15,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Styles.fieldColor(context),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Styles.fieldColor(context),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Styles.selectedItemColor(context),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(18),
                  ),
                ),
                listTileDivider: Divider(
                  color: Styles.fieldColor(context),
                  thickness: 0.4,
                ),
                backgroundColor: Styles.AccentColor(context),
                countryCodeStyle:
                TextStyle(color: Styles.textColor(context)),
                countryNameStyle:
                TextStyle(color: Styles.textColor(context)),
              ),
              keyboardAppearance: Styles.getTheme(context)
                  ? Brightness.dark
                  : Brightness.light,
              disableLengthCheck: true,
              style: TextStyle(
                color: Styles.textColor(context),
              ),
              cursorColor: Styles.textColor(context),
            ),
          ),
          MyTextField(
            autofill: [AutofillHints.fullStreetAddress],
            inputFormatter: [],
            context: context,
            hintText: 'Street Address',
            labelTxt: "Address *",
            icon: Icons.streetview_rounded,
            inputType: TextInputType.streetAddress,
            controlName: "address",
            validationMessages: (control) => {
              ValidationMessage.required: "This field must not be empty.",
              ValidationMessage.pattern:
              'Please enter a valid Street address.'
            },
          ),
          ReactiveFormConsumer(
            builder: (buildContext, form, child) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: form.valid ? onStepContinue: null,
                      icon: Icon(
                        Icons.done_rounded,
                        color: form.valid
                            ? Colors.blue
                            : Styles.iconColor(context),
                        size:30.0,
                      )),
                  SizedBox(width: 50.0,),
                  IconButton(
                      onPressed: onStepCancel,
                      icon: Icon(
                        Icons.close_rounded,
                        color: Colors.blue,
                        size:30.0,
                      )),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
