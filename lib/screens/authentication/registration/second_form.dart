import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_password_field.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

class SecondForm extends GetView<FieldController> {
  const SecondForm({
    Key? key,
    required this.onStepContinue,
    required this.onStepCancel,
  }) : super(key: key);
  final VoidCallback onStepContinue;
  final VoidCallback onStepCancel;
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(()=>FieldController());
    return ReactiveForm(
      formGroup: controller.fieldFormGroup2,
      child: Column(
        children: [
          MyTextField(
            autofill: [AutofillHints.email],
            inputFormatter: [],
            context: context,
            hintText: 'John.doe@gmail.com',
            labelTxt: "Email *",
            icon: Icons.alternate_email_rounded,
            inputType: TextInputType.emailAddress,
            controlName: controller.email,
            validationMessages: (control) => {
              ValidationMessage.required: "This field must not be empty.",
              ValidationMessage.email:
                  "Please make sure that you entered the right email.",
            },
          ),
          new MyPasswordField(
            hintText: 'enter your password.',
            labelTxt: "Password *",
            inputType: TextInputType.text,
            controlName: controller.password,
            validationMsgs: (control) => {
              ValidationMessage.required: "This field must not be empty.",
              ValidationMessage.minLength:
                  "Password must be longer than 8 characters.",
              ValidationMessage.pattern:
              "Password must contain at least one upper case letter,one number and one special character",
            },
          ),
          new MyPasswordField(
            hintText: 'Confirm your password',
            labelTxt: "Confirm your password *",
            inputType: TextInputType.text,
            controlName: controller.confirmPassword,
            validationMsgs: (control) => {
              ValidationMessage.required: "This field must not be empty.",
              ValidationMessage.mustMatch: "the passwords must match.",
            },
          ),
          ReactiveFormConsumer(
            builder: (buildContext, form, child) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: form.valid ? onStepContinue : null,
                      icon: Icon(
                        Icons.done_rounded,
                        color: form.valid
                            ? Colors.blue
                            : Styles.iconColor(context),
                        size:30.0,
                      )),
                  SizedBox(width: 50.0,),
                  IconButton(
                      onPressed: onStepCancel,
                      icon: Icon(
                        Icons.close_rounded,
                        color: Colors.blue,
                        size:30.0,
                      )),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
