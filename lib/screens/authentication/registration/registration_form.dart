import 'package:flutter/material.dart';
import 'package:frontend/models/data/data.dart';
import 'package:frontend/screens/authentication'
    '/registration/first_form.dart';
import 'package:frontend/screens/authentication/registration/fourth_form.dart';
import 'package:frontend/screens/authentication/registration/second_form.dart';
import 'package:frontend/screens/authentication/registration/third_form.dart';
import 'package:frontend/utils/styles.dart';
import 'package:get/get.dart';

class RegistrationForm extends StatefulWidget {
  const RegistrationForm({Key? key}) : super(key: key);

  @override
  State<RegistrationForm> createState() => _RegistrationFormState();
}

class _RegistrationFormState extends State<RegistrationForm> {
  int _index = 0;
late Data data;
String number="";

  List<Step> _mySteps() {
    List<Step> _steps = [
      Step(
        title: Text('Name'),
        content: FirstForm(
          onStepContinue: () {
            _onStepContinue();
          },
        ),
        isActive: _index >= 0,
        state: _index == 0 ? StepState.editing : StepState.indexed,
      ),
      Step(
        title: Text('Account Details'),
        content: SecondForm(
          onStepCancel: () {
            _onStepCancel();
          },
          onStepContinue: () {
            _onStepContinue();
          },
        ),
        isActive: _index >= 1,
        state: _index == 1 ? StepState.editing : StepState.indexed,
      ),
      Step(
        title: Text("Personal information"),
        content: ThirdForm(
          onStepCancel: () {
            _onStepCancel();
          },
          onStepContinue: () {
            _onStepContinue();
          },
        ),
        isActive: _index >= 2,
        state: _index == 2 ? StepState.editing : StepState.indexed,
      ),
      Step(
        title: Text("Identification card"),
        content: FourthForm(
          onStepCancel: () {
            _onStepCancel();
          },
          number:number,
        ),
        isActive: _index >= 3,
        state: _index == 3 ? StepState.editing : StepState.indexed,
      ),
    ];
    return _steps;
  }

  _onStepCancel() {
    if (_index > 0) {
      setState(() {
        _index -= 1;
      });
    }
  }

  _onStepContinue() {
    if (_index <= 2) {
      setState(() {
        _index += 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if(Get.previousRoute == '/Terms'){
      number=Get.arguments['phone'];
    }
    return Scaffold(
      backgroundColor: Styles.bgColor(context),
      appBar: AppBar(
        backgroundColor: Styles.headerColor(context),
        title: const Text('Create An Account'),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Colors.white,
          ),
        ),
      ),
      body: Stepper(
        currentStep: _index,
        physics: ClampingScrollPhysics(),
        controlsBuilder: (BuildContext context, ControlsDetails details) {
          return Row(
            children: <Widget>[],
          );
        },
        steps: _mySteps(),
      /*  onStepTapped: (int index) {
          setState(() {
            _index = index;
          });
        },*/
      ),
    );
  }
}
