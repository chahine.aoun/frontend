import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:frontend/widgets/radio_button.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

class FirstForm extends GetView<FieldController> {
  late String firstName;
  late String lastName;
  late String gender;

  FirstForm({Key? key,required this.onStepContinue,}) : super(key: key);
  final VoidCallback onStepContinue;


  @override
  Widget build(BuildContext context) {
    Get.lazyPut(()=>FieldController());
    return ReactiveFormBuilder(
      form: ()=>controller.fieldFormGroup,
      builder: (context,form,child){
        return Column(
          children: [
            MyTextField(
              autofill: [AutofillHints.givenName],
              inputFormatter: [],
              context: context,
              hintText: 'John',
              labelTxt: "First name *",
              icon: Icons.account_box_rounded,
              inputType: TextInputType.name,
              controlName: "name",
              validationMessages: (control) => {
                ValidationMessage.required: "This field must not be empty.",
                ValidationMessage.pattern:'Please enter a valid name.'
              },
            ),
             MyTextField(
               autofill: [AutofillHints.familyName],
               inputFormatter: [],
               context: context,
              hintText: "Doe",
              labelTxt: 'Last name *',
              icon: Icons.account_box_rounded,
              inputType: TextInputType.name,
              controlName: controller.lastName,
              validationMessages: (control) => {
                ValidationMessage.required: "This field must not be empty.",
                ValidationMessage.pattern:'Please enter a valid last name.'
              },
            ),
            RadioButton(),
            ReactiveFormConsumer(
              builder: (buildContext, form, child) {
                return    Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        onPressed: form.valid ? onStepContinue : null,
                        icon: Icon(
                          Icons.done_rounded,
                          color: form.valid
                              ? Colors.blue
                              : Styles.iconColor(context),
                          size:30.0,
                        )),
                    SizedBox(width: 50.0,),
                    IconButton(
                        onPressed: () {
                          form.updateValue({
                            controller.name: "",
                            controller.lastName: "",
                          });
                        },
                        icon: Icon(
                          Icons.close_rounded,
                          color: Colors.blue,
                          size:30.0,
                        )),
                  ],
                );
              },
            ),
          ],
        );},
    );
  }
}
