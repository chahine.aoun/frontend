import 'package:flutter/material.dart';
import 'package:frontend/controller/field_controller.dart';
import 'package:frontend/screens/authentication/welcome_page.dart';
import 'package:frontend/services/dio_client_authentication.dart';
import 'package:frontend/screens/authentication/otp/otp_view.dart';
import 'package:frontend/screens/authentication/otp/phone_input.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_button.dart';
import 'package:frontend/widgets/my_password_field.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:sms_autofill/sms_autofill.dart';

class LoginPage extends GetView<FieldController> {
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => FieldController());
    final DioClient _client = DioClient();
    return Scaffold(
      backgroundColor: Styles.bgColor(context),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.off(() => WelcomePage());
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      body: SafeArea(
        child: ReactiveForm(
          formGroup: controller.loginFormGroup,
          child: CustomScrollView(
            reverse: true,
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Flexible(
                        fit: FlexFit.loose,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Welcome back.",
                              style: TextStyle(
                                color: Styles.titleColor(context),
                                fontSize: 34,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "You've been missed!",
                              style: TextStyle(
                                  fontFamily: 'Akshar',
                                  fontSize: 28,
                                  fontWeight: FontWeight.w500,
                                  color: Styles.titleColor(context)),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 0),
                                    blurRadius: 0.01,
                                    spreadRadius: 0.01,
                                    color: Colors.black26,
                                  ),
                                ],
                                color: Styles.AccentColor(context),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              child: Column(
                                children: [
                                  MyTextField(
                                    autofill: [AutofillHints.email],
                                    inputFormatter: [],
                                    context: context,
                                    hintText: 'John.doe@gmail.com',
                                    labelTxt: "email *",
                                    icon: Icons.alternate_email_rounded,
                                    inputType: TextInputType.emailAddress,
                                    controlName: controller.email,
                                    validationMessages: (control) => {
                                      ValidationMessage.required:
                                          "This field must not be empty.",
                                      ValidationMessage.email:
                                          "Please make sure that you entered the right email.",
                                    },
                                  ),
                                  Divider(
                                    color: Styles.dividerColor(context),
                                    thickness: 0.8,
                                  ),
                                  new MyPasswordField(
                                    hintText: 'enter your password.',
                                    labelTxt: "Password *",
                                    inputType: TextInputType.text,
                                    controlName: controller.password,
                                    validationMsgs: (control) => {
                                      ValidationMessage.required:
                                          "This field must not be empty.",
                                      ValidationMessage.minLength:
                                          "Password must be longer than 8 characters.",
                                      ValidationMessage.pattern:
                                          "Please enter a valid password.",
                                    },
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 30.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Forgot you password? ",
                                    style: TextStyle(
                                      color: Styles.textColor(context),
                                      fontSize: 15,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(
                                        () => PhoneInput(),
                                        transition: Transition.cupertinoDialog,
                                        duration: Duration(
                                          milliseconds: 400,
                                        ),
                                        arguments: {
                                          'purpose': "password reset"
                                        },
                                      );
                                    },
                                    child: Text('Reset password',
                                        style: TextStyle(
                                          color: Styles.linkColor(context),
                                          fontSize: 15,
                                          decoration: TextDecoration.underline,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      ReactiveFormConsumer(
                        builder: (buildContext, form, child) {
                          return elevatedButton(
                            width: double.infinity,
                            color: Styles.headerColor2(context),
                            context: context,
                            callback: () async {
                              if (form.valid) {
                                String outcome = await _client.login(
                                    login: controller
                                        .loginFormGroup.value["email"]
                                        .toString(),
                                    pass: controller
                                        .loginFormGroup.value["password"]
                                        .toString());
                                if (outcome == "null") {
                                  Map<String, String> outcome2 =
                                      await _client.getNumber(
                                    email: controller
                                        .loginFormGroup.value["email"]
                                        .toString(),
                                  );
                                  await _client.otp(
                                      phoneNumber: outcome2["number"] as String,
                                      appSig:
                                          await SmsAutoFill().getAppSignature);
                                  Get.to(
                                    () => OtpView(),
                                    transition: Transition.rightToLeftWithFade,
                                    duration: Duration(
                                      milliseconds: 200,
                                    ),
                                    arguments: {
                                      'phone': outcome2["number"],
                                      'purpose': 'login'
                                    },
                                  );
                                } else {
                                  if (outcome == "Bad credentials") {
                                    loadingDialog(
                                        context, "Wrong password.");

                                  } else if (outcome == "User is disabled") {
                                    loadingDialog(
                                        context,
                                        "Account wasn't enabled. \n please check your email to enable your account.");
                                  }
                                }
                              }
                            },
                            text: 'Login',
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Don't have an account? ",
                              style: TextStyle(
                                color: Styles.textColor(context),
                                fontSize: 15,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Get.to(
                                  () => PhoneInput(),
                                  transition: Transition.rightToLeftWithFade,
                                  duration: Duration(
                                    milliseconds: 150,
                                  ),
                                  arguments: {'purpose': "registration"},
                                );
                              },
                              child: Text('Register',
                                  style: TextStyle(
                                    color: Styles.linkColor(context),
                                    fontSize: 15,
                                    decoration: TextDecoration.underline,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void loadingDialog(BuildContext context, String txt) async {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return Dialog(
            backgroundColor: Styles.AccentColor(context),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(
                    color: Styles.linkColor(context),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Signing in...',
                    style: TextStyle(color: Styles.whiteAndBlack(context)),
                  )
                ],
              ),
            ),
          );
        });
    await Future.delayed(const Duration(seconds: 2));
    Navigator.of(context).pop();

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            txt,
            style: TextStyle(color: Colors.white),
          ),
          elevation: 1.0,
          backgroundColor: const Color(0xFF024751),
          duration: Duration(seconds: 3),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
        ),
      );

  }
}
