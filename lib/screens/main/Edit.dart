import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:frontend/controller/editing_controller.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/country_picker_dialog.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/my_text_field.dart';
import 'package:intl/intl.dart';
import 'package:reactive_image_picker/image_file.dart';
import 'package:reactive_image_picker/reactive_image_picker.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../models/user_response/userResponse.dart';
import '../../services/dio_client_authentication.dart';
import '../../utils/styles.dart';
import '../../widgets/my_button.dart';
import '../../widgets/my_text_field.dart';
import '../authentication/otp/otp_view.dart';

class Edit extends StatefulWidget {
  final UserResponse user;
  Edit({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  EditState createState() => EditState();
}

class EditState extends State<Edit> {
  final DioClient _client = DioClient();
  bool valueChange = false;

  String number = "";
  bool numberBool = false;
  bool nameBool = true;
  bool lastNameBool = true;
  bool addressBool = true;
  bool birthBool = true;
  bool countryBool = false;
  bool typeBool = true;
  int i = 0;

  final EditingController controller = Get.put(EditingController());
  final TextEditingController textController = TextEditingController(
    text: "Country of Residence",
  );
  final TextEditingController numberController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    initiateForm(controller);
    return ReactiveFormBuilder(
      form: () => controller.editingForm,
      builder: (context, form, child) {
        return Scaffold(
          bottomSheet: Container(
            color: Styles.bgColor(context),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: ReactiveFormConsumer(
              builder: (context, form, child) {
                return elevatedButton(
                  width: double.infinity,
                  color: Styles.selectedItemColor(context),
                  context: context,
                  callback: form.valid && valueChange
                      ? () async {
                          FocusManager.instance.primaryFocus?.unfocus();
                          await Future.delayed(
                              const Duration(milliseconds: 220));
                          UserResponse user = confirmChange();
                          if (!typeBool) {
                            ImageFile image = controller
                                .editingForm.value["image"] as ImageFile;
                            _client.otp(
                                phoneNumber: number,
                                appSig: await SmsAutoFill().getAppSignature);

                            Get.to(
                              () => OtpView(),
                              transition: Transition.rightToLeftWithFade,
                              duration: Duration(
                                milliseconds: 200,
                              ),
                              arguments: {
                                'phone': number,
                                'user': user.toJsonString(),
                                'image': image.image
                                    .toString()
                                    .substring(8)
                                    .replaceAll("'", ""),
                                "fileName": user.firstName +
                                    "_" +
                                    user.lastName.removeAllWhitespace,
                              },
                            );
                          } else {
                            Get.to(
                              () => OtpView(),
                              transition: Transition.rightToLeftWithFade,
                              duration: Duration(
                                milliseconds: 200,
                              ),
                              arguments: {
                                'phone': number,
                                'user': user.toJsonString(),
                              },
                            );
                          }
                        }
                      : null,
                  text: 'Update',
                );
              },
            ),
          ),
          appBar: AppBar(
            title: Text(
              "Edit",
              style: TextStyle(
                color: Styles.titleColor(context),
              ),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: IconButton(
              onPressed: () async {
                FocusManager.instance.primaryFocus?.unfocus();
                await Future.delayed(const Duration(milliseconds: 220));
                Get.back();
              },
              icon: Icon(
                Icons.keyboard_backspace_rounded,
                size: 30.0,
                color: Styles.whiteAndBlack(context),
              ),
            ),
          ),
          backgroundColor: Styles.bgColor(context),
          body: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 15,
            ),
            child: Container(
              height: Get.height * 0.8,
              child: ListView(
                physics: BouncingScrollPhysics(),
                children: [
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: MyTextField(
                          onEditingComplete: () {
                            setState(() {
                              valueChange = true;
                            });
                          },
                          readOnly: nameBool,
                          autofill: [AutofillHints.givenName],
                          inputFormatter: [],
                          context: context,
                          hintText: 'John',
                          labelTxt: "First name",
                          icon: Icons.account_box_rounded,
                          inputType: TextInputType.name,
                          controlName: "name",
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                            ValidationMessage.pattern:
                                'Please enter a valid name.'
                          },
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            nameBool = !nameBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: MyTextField(
                          onEditingComplete: () {
                            setState(() {
                              valueChange = true;
                            });
                          },
                          readOnly: lastNameBool,
                          autofill: [AutofillHints.familyName],
                          inputFormatter: [],
                          context: context,
                          hintText: "Doe",
                          labelTxt: 'Last name',
                          icon: Icons.account_box_rounded,
                          inputType: TextInputType.name,
                          controlName: controller.lastName,
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                            ValidationMessage.pattern:
                                'Please enter a valid last name.'
                          },
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            lastNameBool = !lastNameBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: IntlPhoneField(
                          controller: numberController,
                          showCountryFlag: false,
                          enabled: numberBool,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          decoration: InputDecoration(
                            labelText: 'Phone Number',
                            labelStyle: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 15,
                            ),
                            counterStyle: TextStyle(
                              color: Styles.textColor(context),
                            ),
                            contentPadding: EdgeInsets.all(20),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.selectedItemColor(context),
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                          ),
                          dropdownIcon: const Icon(
                            Icons.arrow_drop_down_rounded,
                            size: 1,
                          ),
                          dropdownTextStyle: TextStyle(
                            color: Styles.textColor(context),
                          ),
                          pickerDialogStyle: PickerDialogStyle(
                            searchFieldCursorColor: Styles.textColor(context),
                            searchFieldInputDecoration: InputDecoration(
                              hintText: "Search",
                              suffixIcon: Icon(
                                Icons.search_rounded,
                                color: Styles.iconColor(context),
                                size: 30,
                              ),
                              hintStyle: TextStyle(
                                color: Styles.subTextColor(context),
                                fontSize: 15,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.fieldColor(context),
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.selectedItemColor(context),
                                  width: 1.8,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.8,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            listTileDivider: Divider(
                              color: Styles.fieldColor(context),
                              thickness: 0.4,
                            ),
                            backgroundColor: Styles.AccentColor(context),
                            countryCodeStyle:
                                TextStyle(color: Styles.textColor(context)),
                            countryNameStyle:
                                TextStyle(color: Styles.textColor(context)),
                          ),
                          keyboardAppearance: Styles.getTheme(context)
                              ? Brightness.dark
                              : Brightness.light,
                          disableLengthCheck: false,
                          invalidNumberMessage:
                              "Please make sure you entered a valid number.",
                          style: TextStyle(
                            color: Styles.textColor(context),
                          ),
                          cursorColor: Styles.textColor(context),
                          initialCountryCode: 'TN',
                          onChanged: (phone) {
                            setState(() async {
                              bool test =
                                  await validNumber(phone.completeNumber);
                              if (test) {
                                number = phone.completeNumber;
                              } else {
                                numberController.clear();
                                number = widget.user.phoneNumber;
                              }
                            });
                          },
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            numberBool = !numberBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: Theme(
                          data: Theme.of(context).copyWith(
                            colorScheme: ColorScheme.light(
                              primary: Styles.AccentColor(
                                  context), // header background color
                              onPrimary: Styles.whiteAndBlack(
                                  context), // header text color
                              onSurface: Styles.whiteAndBlack(
                                  context), // body text color
                            ),
                            dividerColor: Styles.dividerColor(context),
                            dialogTheme: DialogTheme(
                              backgroundColor: Styles.bgColor(context),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                            ),
                            textButtonTheme: TextButtonThemeData(
                              style: TextButton.styleFrom(
                                primary: Styles.whiteAndBlack(
                                    context), // button text color
                              ),
                            ),
                          ),
                          child: ReactiveDatePicker<DateTime>(
                            formControlName: 'date',
                            firstDate: DateTime(1970),
                            lastDate: DateTime.now(),
                            initialDatePickerMode: DatePickerMode.year,
                            builder: (context, picker, child) {
                              return ReactiveTextField(
                                formControlName: 'date',
                                valueAccessor: DateTimeValueAccessor(
                                  dateTimeFormat: DateFormat('dd MMM yyyy'),
                                ),
                                validationMessages: (control) => {
                                  ValidationMessage.required:
                                      "This field must not be empty.",
                                },
                                onTap: !birthBool
                                    ? () {
                                        picker.showPicker();
                                      }
                                    : null,
                                readOnly: true,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.calendar_month_rounded,
                                    size: 15,
                                    color: Styles.iconColor(context),
                                  ),
                                  contentPadding: EdgeInsets.all(20),
                                  labelText: 'Birth date',
                                  hintText: "DD-MM-YYYY",
                                  labelStyle: TextStyle(
                                    color: Styles.textColor(context),
                                    fontSize: 15,
                                  ),
                                  hintStyle: TextStyle(
                                    color: Styles.subTextColor(context),
                                    fontSize: 15,
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Styles.fieldColor(context),
                                      width: 1.5,
                                    ),
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Styles.selectedItemColor(context),
                                      width: 1.8,
                                    ),
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                      width: 1.8,
                                    ),
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.red,
                                      width: 1.5,
                                    ),
                                    borderRadius: BorderRadius.circular(18),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            birthBool = !birthBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Container(
                          width: Get.width * 0.8,
                          child: IntlPhoneField(
                            dropdownIconPosition: IconPosition.trailing,
                            dropdownIcon: const Icon(
                              Icons.arrow_drop_down_rounded,
                              size: 35,
                            ),
                            showCountryFlag: false,
                            enabled: countryBool,
                            controller: textController,
                            showCursor: false,
                            readOnly: true,
                            onCountryChanged: (country) {
                              setState(() {
                                valueChange = true;
                                textController.value = TextEditingValue(
                                  text: country.name,
                                );
                              });
                            },
                            decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.flag_rounded,
                                size: 15,
                                color: Styles.iconColor(context),
                              ),
                              labelText: 'Country Of Residence',
                              labelStyle: TextStyle(
                                color: Styles.textColor(context),
                                fontSize: 15,
                              ),
                              counterStyle: TextStyle(
                                color: Styles.textColor(context),
                              ),
                              contentPadding: EdgeInsets.all(20),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.fieldColor(context),
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.fieldColor(context),
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Styles.selectedItemColor(context),
                                  width: 1.8,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.8,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.red,
                                  width: 1.5,
                                ),
                                borderRadius: BorderRadius.circular(18),
                              ),
                            ),
                            dropdownTextStyle: TextStyle(
                              color: Colors.transparent,
                              fontSize: 0.5,
                            ),
                            pickerDialogStyle: PickerDialogStyle(
                              searchFieldCursorColor: Styles.textColor(context),
                              searchFieldInputDecoration: InputDecoration(
                                hintText: "Search",
                                suffixIcon: Icon(
                                  Icons.search_rounded,
                                  color: Styles.iconColor(context),
                                  size: 30,
                                ),
                                hintStyle: TextStyle(
                                  color: Styles.subTextColor(context),
                                  fontSize: 15,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Styles.fieldColor(context),
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Styles.fieldColor(context),
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Styles.selectedItemColor(context),
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                                errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.red,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(18),
                                ),
                              ),
                              listTileDivider: Divider(
                                color: Styles.fieldColor(context),
                                thickness: 0.4,
                              ),
                              backgroundColor: Styles.AccentColor(context),
                              countryCodeStyle:
                                  TextStyle(color: Styles.textColor(context)),
                              countryNameStyle:
                                  TextStyle(color: Styles.textColor(context)),
                            ),
                            keyboardAppearance: Styles.getTheme(context)
                                ? Brightness.dark
                                : Brightness.light,
                            disableLengthCheck: true,
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                            cursorColor: Styles.textColor(context),
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            countryBool = !countryBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: MyTextField(
                          onEditingComplete: () {
                            setState(() {
                              valueChange = true;
                            });
                          },
                          readOnly: addressBool,
                          autofill: [AutofillHints.fullStreetAddress],
                          inputFormatter: [],
                          context: context,
                          hintText: 'Street Address',
                          labelTxt: "Address",
                          icon: Icons.streetview_rounded,
                          inputType: TextInputType.streetAddress,
                          controlName: "address",
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                            ValidationMessage.pattern:
                                'Please enter a valid Street address.'
                          },
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            addressBool = !addressBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      bottom: 10,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Identification Piece",
                          style: TextStyle(
                            color: Styles.subTextColor(context),
                            fontSize: 15,
                          ),
                        ),
                        Container(
                          width: Get.width * 0.58,
                          child: Divider(
                            thickness: 1,
                            color: Styles.dividerColor(context),
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width * 0.8,
                        child: ReactiveDropdownField(
                          onChanged: (bool) {
                            setState(() {
                              controller.editingForm.updateValue(
                                {
                                  "name": controller.editingForm.value["name"],
                                  "lastName":
                                      controller.editingForm.value["lastName"],
                                  "date": controller.editingForm.value["date"],
                                  "expirationDate": controller
                                      .editingForm.value["expirationDate"],
                                  "address":
                                      controller.editingForm.value["address"],
                                  "countryOfResidence": controller
                                      .editingForm.value["countryOfResidence"],
                                  "idNumber": "",
                                  "referral": "",
                                  "dropdown":
                                      controller.editingForm.value["dropdown"],
                                },
                              );
                            });
                          },
                          iconDisabledColor: Styles.bgColor(context),
                          iconEnabledColor: Styles.bgColor(context),
                          readOnly: typeBool,
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "Please pick you ID card's type.",
                          },
                          borderRadius: BorderRadius.circular(18),
                          dropdownColor: Styles.AccentColor(context),
                          formControlName: 'dropdown',
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.arrow_drop_down_rounded,
                              size: 30,
                              color: !typeBool
                                  ? Styles.fieldColor(context)
                                  : Styles.bgColor(context),
                            ),
                            contentPadding: EdgeInsets.all(20),
                            labelText: 'ID Type',
                            labelStyle: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 15,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.selectedItemColor(context),
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                          ),
                          items: const [
                            DropdownMenuItem<bool>(
                                value: true, child: Text('Passport')),
                            DropdownMenuItem<bool>(
                                value: false, child: Text('ID card')),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          setState(() {
                            typeBool = !typeBool;
                          });
                        },
                        icon: Icon(
                          Icons.mode_edit_outline_rounded,
                          size: 25.0,
                          color: Styles.activeColor(context),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: Get.width * 0.43,
                        child: MyTextField(
                          onEditingComplete: () {
                            setState(() {
                              valueChange = true;
                            });
                          },
                          readOnly: typeBool,
                          inputFormatter: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          context: context,
                          hintText: 'ID Number',
                          labelTxt: "ID Number",
                          icon: Icons.numbers_rounded,
                          inputType: TextInputType.number,
                          controlName: "idNumber",
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                            ValidationMessage.number:
                                'Please enter a valid ID number.',
                            ValidationMessage.minLength:
                                'Please enter a valid ID number.',
                          },
                        ),
                      ),
                      Container(
                        width: Get.width * 0.43,
                        child: MyTextField(
                          onEditingComplete: () {
                            setState(() {
                              valueChange = true;
                            });
                          },
                          readOnly: typeBool,
                          inputFormatter: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          context: context,
                          hintText: 'Referral code',
                          labelTxt: "Referral code ",
                          icon: Icons.numbers_rounded,
                          inputType: TextInputType.number,
                          controlName: "referral",
                          validationMessages: (control) => {},
                        ),
                      ),
                    ],
                  ),
                  Theme(
                    data: Theme.of(context).copyWith(
                      colorScheme: ColorScheme.light(
                        primary: Styles.AccentColor(
                            context), // header background color
                        onPrimary:
                            Styles.whiteAndBlack(context), // header text color
                        onSurface:
                            Styles.whiteAndBlack(context), // body text color
                      ),
                      dividerColor: Styles.dividerColor(context),
                      dialogTheme: DialogTheme(
                        backgroundColor: Styles.bgColor(context),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                      ),
                      textButtonTheme: TextButtonThemeData(
                        style: TextButton.styleFrom(
                          primary: Styles.whiteAndBlack(
                              context), // button text color
                        ),
                      ),
                    ),
                    child: ReactiveDatePicker<DateTime>(
                      formControlName: 'expirationDate',
                      firstDate: DateTime.now(),
                      lastDate: DateTime.utc(DateTime.now().year + 5),
                      initialDatePickerMode: DatePickerMode.year,
                      builder: (context, picker, child) {
                        return ReactiveTextField(
                          formControlName: 'expirationDate',
                          valueAccessor: DateTimeValueAccessor(
                            dateTimeFormat: DateFormat('dd MMM yyyy'),
                          ),
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                          },
                          onTap: !typeBool
                              ? () {
                                  picker.showPicker();
                                }
                              : null,
                          readOnly: true,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.calendar_month_rounded,
                              size: 15,
                              color: Styles.iconColor(context),
                            ),
                            contentPadding: EdgeInsets.all(20),
                            labelText: "ID's expiration date",
                            hintText: "DD-MM-YYYY",
                            labelStyle: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 15,
                            ),
                            hintStyle: TextStyle(
                              color: Styles.subTextColor(context),
                              fontSize: 15,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.selectedItemColor(context),
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 13,
                  ),
                  !typeBool
                      ? ReactiveImagePicker(
                          formControlName: "image",
                          decoration: InputDecoration(
                            labelText: "Image of the ID piece",
                            labelStyle: TextStyle(
                              color: Styles.textColor(context),
                              fontSize: 15,
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.fieldColor(context),
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Styles.selectedItemColor(context),
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.8,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.red,
                                width: 1.5,
                              ),
                              borderRadius: BorderRadius.circular(18),
                            ),
                          ),
                          validationMessages: (control) => {
                            ValidationMessage.required:
                                "This field must not be empty.",
                          },
                        )
                      : const SizedBox(),
                  const SizedBox(
                    height: 25,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void initiateForm(EditingController controller) {
    setState(() {
      if (i < 1) {
        textController.value = TextEditingValue(
          text: widget.user.countryOfResidence,
        );
        number = widget.user.phoneNumber;
        controller.editingForm.updateValue(
          {
            "name": widget.user.firstName,
            "lastName": widget.user.lastName.removeAllWhitespace,
            "date": DateTime.parse(widget.user.BirthDate),
            "expirationDate": DateTime.parse(widget.user.expirationDate),
            "address": widget.user.address,
            "countryOfResidence": widget.user.countryOfResidence,
            "idNumber": widget.user.idNumber,
            "referral": widget.user.referralCode,
            "dropdown": widget.user.typeOfId == 'ID card' ? false : true,
          },
        );
      }
      i = 1;
    });
  }

  Future<bool> validNumber(String number) async {
    String test = await _client.phoneTest(phoneNumber: number);
    if (test == "false") {
      return true;
    } else {
      return false;
    }
  }

  UserResponse confirmChange() {
    var image = controller.editingForm.value["image"];
    bool a = image != null;
    UserResponse user = UserResponse(
      email: widget.user.email,
      firstName: controller.editingForm.value["name"].toString(),
      lastName: controller.editingForm.value["lastName"].toString(),
      phoneNumber: number,
      typeOfId: a
          ? controller.editingForm.value["dropdown"] == true
              ? 'Passport'
              : 'ID card'
          : widget.user.typeOfId,
      idNumber: a
          ? controller.editingForm.value["idNumber"].toString()
          : widget.user.idNumber,
      expirationDate: a
          ? controller.editingForm.value["expirationDate"]
              .toString()
              .substring(0, 10)
          : widget.user.expirationDate,
      BirthDate:
          controller.editingForm.value["date"].toString().substring(0, 10),
      gender: widget.user.gender,
      countryOfResidence: textController.value.text,
      address: controller.editingForm.value["address"].toString(),
      referralCode: controller.editingForm.value["referral"].toString() + " ",
    );
    return user;
  }
}
