import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/screens/authentication/login_page.dart';
import 'package:frontend/screens/main/Password_change.dart';
import 'package:frontend/screens/main/accountInformation.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/custom_list_tile.dart';
import 'package:get/get.dart';

class Dashboard extends StatelessWidget {
  Dashboard({Key? key}) : super(key: key);

  final _storage = const FlutterSecureStorage();

  Future<List<String?>> getInfo() async {
    List<String?> l = [];
    l.add(await _storage.read(key: 'name'));
    l.add(await _storage.read(key: 'email'));
    l.add(await _storage.read(key: 'gender'));
    return l;
  }



  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String?>>(
      future: getInfo(),
      builder: (BuildContext context, AsyncSnapshot<List<String?>> snapshot) {
        if (snapshot.hasData) {
          bool gender = snapshot.data![2].toString() .compareTo( "male")==0;
          return Scaffold(
            backgroundColor: Styles.bgColor(context),
            body: ListView(
              padding: const EdgeInsets.all(15),
              children: [
                const SizedBox(
                  height: 40,
                ),
                Stack(
                  children: [
                    Container(
                      height: 200,
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 140,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Styles.AccentColor(context),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(height: 20,),
                            Center(
                                child: Text(
                                    snapshot.data![0]
                                        .toString()
                                        .capitalizeFirst
                                        .toString(),
                                    style: TextStyle(
                                        color: Styles.textColor(context),
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold))),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(snapshot.data![1].toString(),
                                style: TextStyle(
                                    color: Styles.subTextColor(context))),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 30,
                      right: 30,
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFFE486DD),
                        ),
                        child: Transform.scale(
                          scale: 0.75,
                          child: Image.asset(gender
                              ? 'assets/images/dizzy-brunette-boys-head.png'
                              : 'assets/images/dizzy-womans-head-with-long-hair.png'),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 35,
                ),
                CustomListTile(
                  icon: Icons.person_rounded,
                  color: const Color(0xFFC76CD9),
                  title: 'Profile',
                  context: context,
                  callback: () {
                    Get.to(() => AccountInformation(),);
                  },
                ),
                CustomListTile(
                  icon: Icons.password_rounded,
                  color: const Color(0xFF064c6d),
                  title: 'Password',
                  context: context,
                  callback: () {
                    Get.to(() => PasswordChange(email: snapshot.data![1].toString(),),);
                  },
                ),
                CustomListTile(
                  icon: Icons.dark_mode_rounded,
                  color: const Color(0xFF0536e8),
                  title: 'Dark Mode',
                  isDarkMode: true,
                  context: context,
                ),
                CustomListTile(
                  icon: Icons.logout_rounded,
                  color: const Color(0xFF6b41dc),
                  title: 'Log Out',
                  context: context,
                  callback: () {
                    _storage.deleteAll();
                    Get.offAll(() => LoginPage());
                  },
                ),
              ],
            ),
          );
        }
        return Container(
          child: Center(child: CircularProgressIndicator()),
          color: Styles.bgColor(context),
        );
      },
    );
  }
}
