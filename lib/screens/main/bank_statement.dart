import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../models/bankAccounts/bankAccounts.dart';
import '../../models/transactions/transaction.dart';
import '../../services/api_dio.dart';
import '../../utils/styles.dart';
import '../../utils/save_file.dart';
import '../../widgets/my_button.dart';

class BankStatement extends StatefulWidget {
  const BankStatement({Key? key}) : super(key: key);

  @override
  State<BankStatement> createState() => _BankStatementState();
}

class _BankStatementState extends State<BankStatement> {
  List<Transaction> l = [];
  List<String> b = ["All"];
  List<BankAccounts> bb = [];
  final Api api = Api();
  final _storage = const FlutterSecureStorage();
  bool selected = false;
  String dropDownString = "All";
  late BankAccounts bankAccount;
  String name = " ";
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now();
  final dateFormat = DateFormat('EEE, MMM d, h:mm a');
  double earned = 0;
  double spent = 0;
  double balance=0;
  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(() {
      if (args.value is PickerDateRange) {
        String range =
            '${DateFormat('yyyy-MM-dd').format(args.value.startDate)} -'
            '${DateFormat('yyyy-MM-dd').format(args.value.endDate ?? args.value.startDate)}';
        startDate = DateTime.parse(range.substring(0, 10));
        endDate = DateTime.parse(range.substring(12));
        selected = true;
      } else {
        selected = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: Container(
        color: Styles.bgColor(context),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: elevatedButton(
          width: double.infinity,
          color: Styles.selectedItemColor(context),
          context: context,
          callback: selected
              ? () async {
                  List<Transaction> t = await loadTransactions();
                  if (dropDownString.compareTo("All") != 0) {
                    t = GetAccountTransactions(bankAccount);
                  }
                  if (t.length == 0) {
                    showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (_) {
                          return Dialog(
                            backgroundColor: Styles.AccentColor(context),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 20,
                                horizontal: 20,
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    'No transactions were made during that period.',
                                    style: TextStyle(
                                      color: Styles.whiteAndBlack(context),
                                    ),
                                    textAlign: TextAlign.center,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                    await Future.delayed(const Duration(seconds: 2));
                    Navigator.of(context).pop();
                  } else {
                    generateInvoice(t);
                  }
                }
              : null,
          text: 'Generate Statement',
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          "Bank Statement",
          style: TextStyle(
            color: Styles.titleColor(context),
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      backgroundColor: Styles.bgColor(context),
      body: Padding(
        padding: const EdgeInsets.all(15,),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            Padding(
              padding: const EdgeInsets.all(15.0),
              child:  Text(
                  "Pick a date range:",
                  style: TextStyle(
                    color: Styles.titleColor(context),
                    fontSize: 17,
                  ),
                ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Styles.AccentColor(context),
            borderRadius: BorderRadius.circular(20),
            ),
              padding:const EdgeInsets.all(10,),
              child: SfDateRangePicker(
                maxDate: DateTime.now(),
                navigationMode: DateRangePickerNavigationMode.scroll,
                view: DateRangePickerView.year,
                selectionMode: DateRangePickerSelectionMode.range,
                onSelectionChanged: _onSelectionChanged,
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(
                "Pick a bank Account:",
                style: TextStyle(
                  color: Styles.titleColor(context),
                  fontSize:17,
                ),
              ),
            ),
            const SizedBox(height: 5,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20,),
              child: FutureBuilder<void>(
                future: loadAccounts(),
                builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                  return Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Styles.fieldColor(context),
                      ),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: new DropdownButton<String>(
                      hint: Text(
                        dropDownString,
                        style: TextStyle(
                          color: Styles.textColor(context),
                          fontSize: 14,
                        ),
                      ),
                      borderRadius: BorderRadius.circular(20),
                      dropdownColor: Styles.AccentColor(context),
                      underline: Container(),
                      icon: Icon(Icons.arrow_drop_down_rounded,
                          color: Styles.whiteAndBlack(context)),
                      items: b.map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String? value) {
                        setState(
                          () {
                            dropDownString = value!;
                            bb.forEach(
                              (element) {
                                if (element.code.toString().compareTo(dropDownString
                                        .substring(dropDownString.indexOf("/")+1,),) ==
                                    0) {
                                  bankAccount = element;
                                  balance=element.balance;
                                }
                              },
                            );
                          },
                        );
                      },
                    ),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 130,
            )
          ],
        ),
      ),
    );
  }

  Future<void> loadAccounts() async {
    balance=0;
    name = (await _storage.read(key: 'name'))!;
    b = ["All"];
    bb = await api.loadBankAccounts();
    bb.forEach((element) {
      if (element.active) {
        b.add(element.type.type + "/" + element.code.toString());
        balance=element.balance+balance;
      }
    });
  }

  Future<List<Transaction>> loadTransactions() async {
    l = [];
    List<Transaction> ll = await api.loadTransactions();
    ll.forEach((element) {
      if (element.date.isAfter(startDate) && element.date.isBefore(endDate) ||
          (element.date.year == startDate.year &&
              element.date.month == startDate.month &&
              element.date.day == startDate.day) ||
          (element.date.year == endDate.year &&
              element.date.month == endDate.month &&
              element.date.day == endDate.day)) {
        l.add(element);
      }
    });
    return l;
  }

  List<Transaction> GetAccountTransactions(BankAccounts b) {
    List<Transaction> m=[];
    l.forEach((element) {
      if (element.sender == b.code || element.receiver == b.code) {
        m.add(element);
      }
    });
    return m;
  }

  //https://github.com/syncfusion/flutter-widgets/tree/master/packages/syncfusion_flutter_pdf/example
  Future<void> generateInvoice(List<Transaction> list) async {
    final PdfDocument document = PdfDocument();
    final PdfPage page = document.pages.add();
    final Size pageSize = page.getClientSize();
    page.graphics.drawRectangle(
        bounds: Rect.fromLTWH(0, 0, pageSize.width, pageSize.height),
        pen: PdfPen(PdfColor(142, 170, 219)));
    final PdfGrid grid = getGrid(list);
    final PdfLayoutResult result = drawHeader(page, pageSize, grid);
    drawGrid(page, grid, result);
    final List<int> bytes = document.saveSync();
    document.dispose();
    await saveAndLaunchFile(bytes, 'bank_statement.pdf');
  }

  PdfLayoutResult drawHeader(PdfPage page, Size pageSize, PdfGrid grid) {
    page.graphics.drawRectangle(
        brush: PdfSolidBrush(PdfColor(91, 126, 215)),
        bounds: Rect.fromLTWH(0, 0, pageSize.width - 115, 90));
    page.graphics.drawString(
        'BANK STATEMENT', PdfStandardFont(PdfFontFamily.helvetica, 30),
        brush: PdfBrushes.white,
        bounds: Rect.fromLTWH(25, 0, pageSize.width - 115, 90),
        format: PdfStringFormat(lineAlignment: PdfVerticalAlignment.middle));

    page.graphics.drawRectangle(
        bounds: Rect.fromLTWH(400, 0, pageSize.width - 400, 90),
        brush: PdfSolidBrush(PdfColor(65, 104, 205)));

    page.graphics.drawString(
        (earned - spent) > 0
            ? r'$' + (earned - spent).toString()
            : r'-$' + (spent - earned).toString(),
        PdfStandardFont(PdfFontFamily.helvetica, 18),
        bounds: Rect.fromLTWH(400, 0, pageSize.width - 400, 100),
        brush: PdfBrushes.white,
        format: PdfStringFormat(
            alignment: PdfTextAlignment.center,
            lineAlignment: PdfVerticalAlignment.middle));

    final PdfFont contentFont = PdfStandardFont(PdfFontFamily.helvetica, 9);
    page.graphics.drawString('Total', contentFont,
        brush: PdfBrushes.white,
        bounds: Rect.fromLTWH(400, 0, pageSize.width - 400, 33),
        format: PdfStringFormat(
            alignment: PdfTextAlignment.center,
            lineAlignment: PdfVerticalAlignment.bottom));
    final DateFormat format = DateFormat.yMMMMd('en_US');
    final String invoiceNumber =
        'Bank Statement Number: 2058557939\r\n\r\nDate: ${format.format(DateTime.now())}';
    final Size contentSize = contentFont.measureString(invoiceNumber);
    String address = 'Bill To: \r\n $name';

    PdfTextElement(text: invoiceNumber, font: contentFont).draw(
        page: page,
        bounds: Rect.fromLTWH(pageSize.width - (contentSize.width + 30), 120,
            contentSize.width + 30, pageSize.height - 120));

    return PdfTextElement(text: address, font: contentFont).draw(
        page: page,
        bounds: Rect.fromLTWH(30, 120,
            pageSize.width - (contentSize.width + 30), pageSize.height - 120))!;
  }

  void drawGrid(PdfPage page, PdfGrid grid, PdfLayoutResult result) {
    Rect? totalPriceCellBounds;
    Rect? quantityCellBounds;
    grid.beginCellLayout = (Object sender, PdfGridBeginCellLayoutArgs args) {
      final PdfGrid grid = sender as PdfGrid;
      if (args.cellIndex == grid.columns.count - 1) {
        totalPriceCellBounds = args.bounds;
      } else if (args.cellIndex == grid.columns.count - 2) {
        quantityCellBounds = args.bounds;
      }
    };
    result = grid.draw(
        page: page, bounds: Rect.fromLTWH(0, result.bounds.bottom + 40, 0, 0))!;
  }

  PdfGrid getGrid(List<Transaction> list) {
    final PdfGrid grid = PdfGrid();
    spent = 0;
    earned = 0;
    grid.columns.add(count: 5);

    final PdfGridRow headerRow = grid.headers.add(1)[0];
    headerRow.style.backgroundBrush = PdfSolidBrush(PdfColor(68, 114, 196));
    headerRow.style.textBrush = PdfBrushes.white;
    headerRow.cells[0].value = 'My Bank Account';
    headerRow.cells[0].stringFormat.alignment = PdfTextAlignment.center;
    headerRow.cells[1].value = 'Others Bank Account';
    headerRow.cells[2].value = 'Date';
    headerRow.cells[3].value = 'Transaction Type';
    headerRow.cells[4].value = 'Amount';

    list.forEach((element) {
      if (element.sent) {
        addProducts(
            element.sender.toString(),
            element.receiver.toString(),
            dateFormat.format(element.date).toString(),
            "Sent",
            element.amount,
            grid);
        spent = spent + element.amount;
      } else {
        addProducts(
            element.receiver.toString(),
            element.sender.toString(),
            dateFormat.format(element.date).toString(),
            "Received",
            element.amount,
            grid);
        earned = earned + element.amount;
      }
    });
    addProducts("", "", "", "Total Spent", spent, grid);
    addProducts("", "", "", "Total Earned", earned, grid);
    addProducts("", "", "", "Balance the "+startDate.toString().substring(0,11), (balance-earned+spent).roundToDouble(), grid);
    addProducts("", "", "", "Balance the "+endDate.toString().substring(0,11), balance.toPrecision(2), grid);

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);

    grid.columns[1].width = 200;
    for (int i = 0; i < headerRow.cells.count; i++) {
      headerRow.cells[i].style.cellPadding =
          PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
    }
    for (int i = 0; i < grid.rows.count; i++) {
      final PdfGridRow row = grid.rows[i];
      for (int j = 0; j < row.cells.count; j++) {
        final PdfGridCell cell = row.cells[j];
        if (j == 0) {
          cell.stringFormat.alignment = PdfTextAlignment.center;
        }
        cell.style.cellPadding =
            PdfPaddings(bottom: 5, left: 5, right: 5, top: 5);
      }
    }
    return grid;
  }

  void addProducts(String productId, String productName, String price,
      String quantity, double total, PdfGrid grid) {
    final PdfGridRow row = grid.rows.add();
    row.cells[0].value = productId;
    row.cells[1].value = productName;
    row.cells[2].value = price.toString();
    row.cells[3].value = quantity.toString();
    row.cells[4].value = total.toString();
  }
}
