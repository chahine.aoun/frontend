import 'dart:io';

import 'package:flutter/material.dart';
import 'package:frontend/screens/main/Transaction_screen.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../models/bankAccounts/bankAccounts.dart';

class QRScanner extends StatefulWidget {
  final BankAccounts acc;
  const QRScanner({Key? key, required this.acc}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRScanner();
}

class _QRScanner extends State<QRScanner> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool _flashOn = false;
  bool _frontCam = false;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void readQr() async {
    if (result != null) {
      controller!.pauseCamera();
      controller!.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {

    readQr();
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          QRView(
              key: qrKey,
              overlay: QrScannerOverlayShape(borderColor: Color(0xFFE1C884)),
              onQRViewCreated: (QRViewController controller) {
                this.controller = controller;
                controller.scannedDataStream.listen((scanData) {
                  print(scanData);
                  if (mounted) {
                    result = scanData;
                    controller.dispose();
                    Get.off(()=>TransactionScreen(acc: widget.acc),arguments: {'result':result!.code});
                  }
                });
              }),
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: EdgeInsets.only(
                top: 50,
              ),
              child: Text(
                'QR code scanner',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: ButtonBar(
                alignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _flashOn = !_flashOn;
                      });
                      controller?.toggleFlash();
                    },
                    icon: Icon(_flashOn
                        ? Icons.flash_on_rounded
                        : Icons.flash_off_rounded),
                    color: Colors.white,
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _frontCam = !_frontCam;
                      });
                      controller?.flipCamera();
                    },
                    icon: Icon(_frontCam
                        ? Icons.camera_front_rounded
                        : Icons.camera_rear_rounded),
                    color: Colors.white,
                  ),
                  IconButton(
                    onPressed: () {
                      controller!.dispose();
                      Get.back();
                    },
                    icon: Icon(Icons.exit_to_app),
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

}
