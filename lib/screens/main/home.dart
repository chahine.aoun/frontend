import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:frontend/models/bankAccounts/bankAccounts.dart';
import 'package:frontend/screens/authentication/create_bank_account.dart';
import 'package:frontend/screens/main/card_dashboard.dart';
import 'package:frontend/services/api_dio.dart';
import 'package:frontend/utils/styles.dart';

import 'package:frontend/widgets/card.dart';
import 'package:frontend/widgets/transaction_rep.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../models/transactions/transaction.dart';
import '../../widgets/Drawer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Home createState() => Home();
}

class Home extends State<HomeScreen> {
  final controller = PageController();
  final controller2 = PageController();
  final Api api = Api();
  List<Transaction> l = [];
  List<Transaction> ll = [];
  List<double> expenses = [];
  final List<String> dropDown = <String>["All", "Today", "Week", "Month"];
  String order = "All";
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: MyDrawer(
        context: context,
      ),
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Styles.whiteAndBlack(context),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          "Home",
          style: TextStyle(color: Styles.textColor(context), fontSize: 18),
        ),
        centerTitle: true,
      ),
      backgroundColor: Styles.bgColor(context),
      body: FutureBuilder<List<BankAccounts>>(
        future: loadAccounts(),
        builder:
            (BuildContext context, AsyncSnapshot<List<BankAccounts>> snapshot) {
          if (snapshot.hasData) {
            expenses = getExpenses();
            return CustomScrollView(
              reverse: true,
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        height: 200.0,
                        child: ListView.separated(
                          physics: BouncingScrollPhysics(),
                          controller: controller,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            if (index == snapshot.data!.length) {
                              var size = MediaQuery.of(context).size;
                              return Container(
                                width: size.width * 0.6,
                                decoration: BoxDecoration(
                                  color: Colors.transparent.withOpacity(0.02),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                child: IconButton(
                                  onPressed: () {
                                    Get.to(
                                      () => CreateBankAccount(),
                                      transition: Transition.cupertinoDialog,
                                      duration: Duration(milliseconds: 60),
                                    );
                                  },
                                  icon: Icon(
                                    Icons.add_circle_outline_rounded,
                                    size: 100.0,
                                    color: Styles.selectedItemColor(context),
                                  ),
                                ),
                              );
                            }
                            return GestureDetector(
                              onTap: () {
                                List<Transaction> ll = [];
                                l.forEach((element) {
                                  if (element.sender ==
                                          snapshot.data![index].code ||
                                      element.receiver ==
                                          snapshot.data![index].code) {
                                    ll.add(element);
                                  }
                                });
                                Get.to(
                                  () => CardDashboard(
                                    ll: GetAccountTransactions(
                                        snapshot.data![index]),
                                    acc: snapshot.data![index],
                                  ),
                                  transition: Transition.cupertinoDialog,
                                );
                              },
                              child: MyCard(
                                acc: snapshot.data![index],
                              ),
                            );
                          },
                          separatorBuilder: (context, _) => SizedBox(
                            width: 15.0,
                          ),
                          itemCount: snapshot.data!.length + 1,
                        ),
                      ),
                      SmoothPageIndicator(
                        controller: controller,
                        count: snapshot.data!.length + 1,
                        effect: ScrollingDotsEffect(
                          spacing: 3.0,
                          dotWidth: 010.0,
                          dotHeight: 10.0,
                          strokeWidth: 1.5,
                          dotColor: Styles.fieldColor(context),
                          activeDotColor: Styles.selectedItemColor(context),
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: size.height * 0.13,
                                width: size.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Styles.AccentColor(context),
                                  borderRadius: BorderRadius.circular(32.0),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Total balance',
                                      style: TextStyle(
                                        color: Styles.textColor(context),
                                      ),
                                    ),
                                    const SizedBox(height: 8.0),
                                    Text(
                                      '\$ ' +
                                          getBalance(snapshot.data!)
                                              .toPrecision(3)
                                              .toString(),
                                      style: TextStyle(
                                        color: Styles.textColor(context),
                                      ),
                                    ),
                                    const SizedBox(height: 8.0),
                                  ],
                                ),
                              ),
                              Container(
                                height: size.height * 0.13,
                                width: size.width * 0.4,
                                decoration: BoxDecoration(
                                  color: Styles.AccentColor(context),
                                  borderRadius: BorderRadius.circular(32.0),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Expenses',
                                      style: TextStyle(
                                        color: Styles.textColor(context),
                                      ),
                                    ),
                                    const SizedBox(height: 8.0),
                                    Text(
                                      expenses[0] > 0
                                          ? '+ \$ ' +
                                              expenses[0]
                                                  .toPrecision(3)
                                                  .toString()
                                          : '- \$ ' +
                                              expenses[0]
                                                  .toString()
                                                  .substring(1),
                                      style: TextStyle(
                                        color: Styles.textColor(context),
                                      ),
                                    ),
                                    const SizedBox(height: 8.0),
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 32.0, vertical: 4.0),
                                      height: 40.0,
                                      child: Sparkline(
                                        cubicSmoothingFactor: 0.2,
                                        useCubicSmoothing: true,
                                        data: expenses.sublist(1),
                                        lineWidth: 3.5,
                                        lineColor: Color(0xFFEA5EB8),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 15.0,
                              vertical: 5.0,
                            ),
                            child: Text(
                              "Transactions",
                              style: TextStyle(
                                color: Styles.textColor(context),
                                fontSize: 14,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 15.0,
                            ),
                            child: new DropdownButton<String>(
                              hint: Text(
                                order,
                                style: TextStyle(
                                  color: Styles.textColor(context),
                                  fontSize: 14,
                                ),
                              ),
                              borderRadius: BorderRadius.circular(20),
                              dropdownColor: Styles.AccentColor(context),
                              underline: Container(),
                              icon: Icon(Icons.arrow_drop_down_rounded,
                                  color: Styles.whiteAndBlack(context)),
                              items: dropDown.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (String? value) {
                                setState(
                                  () {
                                    order = value!;
                                  },
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      l.length == 0
                          ? Padding(
                              padding: const EdgeInsets.all(100),
                              child: Text(
                                "No Transactions",
                                style: TextStyle(
                                  color: Styles.textColor(context),
                                  fontSize: 14,
                                ),
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 15.0,
                              ),
                              child: Container(
                                height: 296,
                                child: ListView.separated(
                                  physics: BouncingScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  itemBuilder: (context, index) {
                                    return TransactionRep(
                                      amount: l[index].amount,
                                      date: l[index].date,
                                      icon: l[index].sent,
                                      title: l[index].sent
                                          ? l[index].receiver.toString()
                                          : l[index].sender.toString(),
                                    );
                                  },
                                  separatorBuilder: (context, _) => Divider(
                                    indent: 100,
                                    endIndent: 100,
                                    color: Styles.dividerColor(context),
                                    height: 1,
                                    thickness: 0.5,
                                  ),
                                  itemCount: l.length,
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            );
          }
          return Container(
            child: Center(child: CircularProgressIndicator()),
            color: Styles.bgColor(context),
          );
        },
      ),
    );
  }

  Future<List<BankAccounts>> loadAccounts() async {
    ll = await api.loadTransactions();
    l = getTransactions(order);
    var accounts = await api.loadBankAccounts();
    accounts.forEach(
      (element) {
        api.refreshAccount(element.code);
      },
    );
    return accounts;
  }

  List<Transaction> getTransactions(String order) {
    List<Transaction> k = [];
    DateTime now = DateTime.now();
    switch (order) {
      case "Today":
        {
          k = [];
          ll.forEach((element) {
            if (element.date.day == now.day) {
              k.insert(0, element);
            }
          });
        }
        break;

      case "Week":
        {
          k = [];
          ll.forEach((element) {
            if (element.date.month == now.month &&
                element.date.year == now.year) {
              if (now.day <= 7) {
                if (element.date.day < 7) {
                  k.add(element);
                }
              } else if (now.day <= 14 && now.day >= 8) {
                if (element.date.day >= 8 && element.date.day <= 14) {
                  k.add(element);
                }
              } else if (now.day <= 21 && now.day >= 15) {
                if (element.date.day >= 15 && element.date.day <= 21) {
                  k.add(element);
                }
              } else if (now.day <= 28 && now.day >= 22) {
                if (element.date.day >= 22 && element.date.day <= 28) {
                  k.add(element);
                }
              } else {
                k.add(element);
              }
            }
          });
        }
        break;
      case "Month":
        {
          k = [];
          ll.forEach((element) {
            if (element.date.month == now.month) {
              k.add(element);
            }
          });
        }
        break;

      default:
        {
          k = ll;
        }
        break;
    }
    return k;
  }

  double getBalance(List<BankAccounts> b) {
    double total = 0;
    b.forEach((element) {
      total = total + element.balance;
    });
    return total;
  }

  List<double> getExpenses() {
    double total = 0;
    List<double> e = [0];
    l.forEach((element) {
      if (element.sent) {
        total = total - element.amount;
        e.insert(0, -element.amount);
      } else {
        total = total + element.amount;
        e.insert(0, element.amount);
      }
    });
    e.insert(0, total);
    e.insert(1, 0);
    return e;
  }

  List<Transaction> GetAccountTransactions(BankAccounts b) {
    List<Transaction> m = [];
    ll.forEach((element) {
      if (element.sender == b.code || element.receiver == b.code) {
        m.add(element);
      }
    });
    return m;
  }
}
