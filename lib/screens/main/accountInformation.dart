import 'package:flutter/material.dart';
import 'package:frontend/screens/main/Edit.dart';
import 'package:get/get.dart';

import '../../models/user_response/userResponse.dart';
import '../../services/api_dio.dart';
import '../../utils/styles.dart';

class AccountInformation extends StatelessWidget {
  AccountInformation({Key? key}) : super(key: key);
  final Api api = Api();
  final TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      reverse: true,
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: Scaffold(
            appBar: AppBar(
              title: Text(
                "Profile",
                style: TextStyle(
                  color: Styles.titleColor(context),
                ),
              ),
              centerTitle: true,
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.keyboard_backspace_rounded,
                  size: 30.0,
                  color: Styles.whiteAndBlack(context),
                ),
              ),
            ),
            backgroundColor: Styles.bgColor(context),
            body: FutureBuilder<UserResponse?>(
              future: loadProfile(),
              builder: (BuildContext context,
                  AsyncSnapshot<UserResponse?> snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      Container(
                        height: 100,
                        width: 100,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFFE486DD),
                        ),
                        child: Transform.scale(
                          scale: 0.75,
                          child: Image.asset(snapshot.data!.gender.compareTo("male")==0
                              ? 'assets/images/dizzy-brunette-boys-head.png'
                              : 'assets/images/dizzy-womans-head-with-long-hair.png'),
                        ),
                      ),
                      SizedBox(
                        height: Get.height * 0.04,
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                          top: 40,
                          left: 15.0,
                          right: 15.0,
                        ),
                        width: double.infinity,
                        height: Get.height * 0.721,
                        decoration: BoxDecoration(
                          color: Styles.AccentColor(context),
                          borderRadius: BorderRadiusDirectional.only(
                            topStart: Radius.circular(40),
                            topEnd: Radius.circular(40),
                          ),
                        ),
                        child: CustomScrollView(
                          reverse: true,
                          slivers: [
                            SliverFillRemaining(
                              hasScrollBody: false,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  getWidget(
                                    "Full name",
                                    snapshot.data!.firstName +" "+
                                        snapshot.data!.lastName.removeAllWhitespace,
                                    context,
                                  ),
                                  getWidget(
                                    "Email Address",
                                    snapshot.data!.email.replaceRange(
                                        2,
                                        snapshot.data!.email.indexOf("@"),
                                        "*" *
                                            (snapshot.data!.email.indexOf("@") -
                                                2)),
                                    context,
                                  ),
                                  getWidget(
                                    "Phone Number",
                                    snapshot.data!.phoneNumber.replaceRange(
                                      7,
                                      snapshot.data!.phoneNumber.length,
                                      "*" *
                                          (snapshot.data!.phoneNumber.length -
                                              7),
                                    ),
                                    context,
                                  ),
                                  getWidget(
                                    "Date Of Birth",
                                    snapshot.data!.BirthDate,
                                    context,
                                  ),
                                  getWidget(
                                    "Address",
                                    snapshot.data!.address +
                                        " ," +
                                        snapshot.data!.countryOfResidence,
                                    context,
                                  ),
                                  getWidget(
                                    snapshot.data!.typeOfId,
                                    "N: " +
                                        snapshot.data!.idNumber.replaceRange(
                                          3,
                                          snapshot.data!.idNumber.length,
                                          "*" *
                                              (snapshot.data!.idNumber.length -
                                                  3),
                                        ) +
                                        "   EXP: " +
                                        snapshot.data!.expirationDate,
                                    context,
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Center(
                                    child: Container(
                                      height: 35,
                                      width: Get.width * 0.3,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: const Color(0xFF024751),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          Get.to(() => Edit(
                                                user: snapshot.data!,
                                              ));
                                        },
                                        icon: Icon(
                                          Icons.mode_edit_outline_rounded,
                                          size: 20.0,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }
                return Container(
                  child: Center(child: CircularProgressIndicator()),
                  color: Styles.bgColor(context),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Future<UserResponse?> loadProfile() async {
    return await api.getUser();
  }

  Widget getWidget(String title, String text, BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 18.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  color: Styles.subTextColor(context),
                  fontSize: 15,
                ),
              ),
              const SizedBox(
                height: 8.0,
              ),
              Text(
                text,
                style: TextStyle(
                  color: Styles.textColor(context),
                  fontWeight: FontWeight.w400,
                  fontSize: 18,
                ),
              ),
            ],
          ),
        ),
        Divider(
          thickness: 0.6,
          color: Styles.dividerColor(context),
        ),
      ],
    );
  }
}
