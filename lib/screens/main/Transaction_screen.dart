import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:frontend/screens/main/QrScanner.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/bottom_nav.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../controller/field_controller.dart';
import '../../models/bankAccounts/bankAccounts.dart';
import '../../services/api_dio.dart';
import '../../widgets/my_button.dart';
import '../../widgets/my_text_field.dart';

class TransactionScreen extends GetView<FieldController> {
  final BankAccounts acc;

  TransactionScreen({Key? key, required this.acc}) : super(key: key);

  final Api api = Api();
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => FieldController());
    if (Get.previousRoute.toString().contains('/QRScanner')) {
      controller.transactionFormGroup.updateValue({
        'amount': controller.transactionFormGroup.value['amount'],
        'pin': controller.transactionFormGroup.value['pin'],
        'email': Get.arguments['result']
            .toString()
            .substring(Get.arguments['result'].toString().indexOf("/") + 1),
        'receiver': Get.arguments['result']
            .toString()
            .substring(0, Get.arguments['result'].toString().indexOf("/")),
      });
      controller.transactionFormGroup.markAsTouched();
    }
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      backgroundColor: Styles.bgColor(context),
      body: ReactiveForm(
        formGroup: controller.transactionFormGroup,
        child: ListView(
          padding: const EdgeInsets.all(15),
          children: [
            Column(
              children: [
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Styles.headerColor2(context),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                              child: Icon(
                                Icons.credit_card_rounded,
                                color: Styles.headerColor2(context),
                                size: 20,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 30.0,
                              ),
                              child: Text(
                                '\$ ' + acc.balance.toPrecision(2).toString(),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 21,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        Text(
                          'ACCOUNT NUMBER',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                          ),
                        ),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          acc.code.toString(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 80,
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Styles.bgColor(context),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: Get.size.width * 0.73,
                            child: MyTextField(
                              autofill: [],
                              inputFormatter: [
                                FilteringTextInputFormatter.digitsOnly,
                              ],
                              context: context,
                              hintText: '123456789',
                              labelTxt: "Send to *",
                              icon: Icons.credit_card_rounded,
                              inputType: TextInputType.number,
                              controlName: controller.receiver,
                              validationMessages: (control) => {
                                ValidationMessage.required:
                                    "This field must not be empty.",
                                ValidationMessage.number:
                                    "Please make sure that you entered the right credit card number.",
                                ValidationMessage.maxLength:
                                    "Account number must be 9 digit long.",
                                ValidationMessage.minLength:
                                    "Account number must be 9 digit long.",
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Styles.bgColor(context),
                              ),
                              padding: EdgeInsets.all(2),
                              child: IconButton(
                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  SystemChannels.textInput
                                      .invokeMethod('TextInput.hide');
                                  Get.to(() => QRScanner(
                                        acc: acc,
                                      ));
                                },
                                icon: Icon(
                                  Icons.qr_code_scanner_rounded,
                                  size: 35,
                                  color: Styles.whiteAndBlack(context),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MyTextField(
                        autofill: [AutofillHints.email],
                        inputFormatter: [],
                        context: context,
                        hintText: 'John.doe@gmail.com',
                        labelTxt: "email *",
                        icon: Icons.alternate_email_rounded,
                        inputType: TextInputType.emailAddress,
                        controlName: controller.email,
                        validationMessages: (control) => {
                          ValidationMessage.required:
                              "This field must not be empty.",
                          ValidationMessage.email:
                              "Please make sure that you entered the right email.",
                        },
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MyTextField(
                        autofill: [],
                        inputFormatter: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                        obscure: true,
                        context: context,
                        hintText: '****',
                        labelTxt: "Pin code *",
                        icon: Icons.lock_rounded,
                        inputType: TextInputType.number,
                        controlName: controller.pin,
                        validationMessages: (control) => {
                          ValidationMessage.required:
                              "This field must not be empty.",
                          ValidationMessage.number:
                              "Please make sure that you entered the right pin code.",
                          ValidationMessage.maxLength:
                              "Pin code must be 4 digit long.",
                          ValidationMessage.minLength:
                              "Pin code must be 4 digit long.",
                        },
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      MyTextField(
                        autofill: [],
                        inputFormatter: [],
                        context: context,
                        hintText: '0.00',
                        labelTxt: "Amount *",
                        icon: Icons.attach_money_rounded,
                        inputType: TextInputType.number,
                        controlName: controller.amount,
                        validationMessages: (control) => {
                          ValidationMessage.required:
                              "This field must not be empty.",
                          ValidationMessage.pattern:
                              "Please make sure that you entered the right amount.",
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 100,
                ),
                ReactiveFormConsumer(
                  builder: (buildContext, form, child) {
                    return elevatedButton(
                      color: Styles.selectedItemColor(context),
                      context: context,
                      callback: form.valid
                          ? () async {
                              String message = await api.checkTransaction(
                                  form.value["receiver"].toString(),
                                  form.value['email'].toString());
                              if (message.compareTo(
                                      "check your email/code combination.") ==
                                  0) {
                                showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    backgroundColor:
                                        Styles.AccentColor(context),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18)),
                                    content: Text(
                                      message,
                                      textAlign: TextAlign.center,
                                    ),
                                    contentPadding: const EdgeInsets.all(30),
                                  ),
                                );
                                await Future.delayed(
                                    const Duration(seconds: 2));
                                Navigator.of(context).pop();
                              } else {
                                showDialog(
                                  context: context,
                                  builder: (context) => AlertDialog(
                                    backgroundColor:
                                        Styles.AccentColor(context),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18)),
                                    content: Text(
                                      "Your are sending \$" +
                                          form.value["amount"].toString() +
                                          " to " +
                                          message +
                                          ".",
                                      textAlign: TextAlign.center,
                                    ),
                                    contentPadding: const EdgeInsets.all(30),
                                    actions: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          elevatedButton(
                                            color: Styles.selectedItemColor(
                                                context),
                                            context: context,
                                            text: "Confirm",
                                            width: 120.0,
                                            callback: () async {
                                              String outcome =
                                                  await api.sendMoney(
                                                      acc.code.toString(),
                                                      form.value['receiver']
                                                          .toString(),
                                                      form.value['amount']
                                                          .toString());
                                              if (outcome
                                                  .contains("successful")) {
                                                Get.offAll(() => BottomNav());
                                              } else {
                                                Navigator.of(context).pop();
                                              }
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(
                                                SnackBar(
                                                  content: Text(
                                                    outcome,
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  elevation: 1.0,
                                                  backgroundColor:
                                                      const Color(0xFF024751),
                                                  duration:
                                                      Duration(seconds: 2),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              3)),
                                                ),
                                              );
                                            },
                                          ),
                                          elevatedButton(
                                            color: Styles.selectedItemColor(
                                                context),
                                            context: context,
                                            width: 120.0,
                                            text: 'Cancel',
                                            callback: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                );
                              }
                            }
                          : null,
                      text: 'Send Money',
                      width: double.infinity,
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
