import 'package:flutter/material.dart';
import 'package:frontend/models/bankAccounts/bankAccounts.dart';
import 'package:frontend/screens/authentication/create_bank_account.dart';
import 'package:frontend/services/api_dio.dart';
import 'package:frontend/utils/styles.dart';
import 'package:frontend/widgets/Drawer.dart';
import 'package:frontend/widgets/card.dart';
import 'package:get/get.dart';

class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);
  @override
  WalletState createState() => WalletState();
}

class WalletState extends State<Wallet> {
  final controller = PageController();
  final Api api = Api();
  final List<String> dropDown = <String>["Balance", "Type", "Inactive"];
  String orderBy = "Balance";
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: MyDrawer(context: context),
      appBar: AppBar(
        centerTitle:true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          "Wallet",
          style: TextStyle(color: Styles.textColor(context), fontSize: 18),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
            ),
            child: new DropdownButton<String>(
              borderRadius: BorderRadius.circular(20),
              dropdownColor: Styles.accentColor2(context),
              underline: Container(),
              icon: Icon(Icons.sort, color: Styles.whiteAndBlack(context)),
              items: dropDown.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (String? value) {
                setState(
                  () {
                    orderBy = value!;
                  },
                );
              },
            ),
          ),
        ],
      ),
      backgroundColor: Styles.bgColor(context),
      body: FutureBuilder<List<BankAccounts>>(
        future: loadAccounts(orderBy),
        builder:
            (BuildContext context, AsyncSnapshot<List<BankAccounts>> snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: [
                InkWell(
                  onTap: () => Get.to(()=>
                    CreateBankAccount(),
                    transition: Transition.cupertinoDialog,
                    duration: Duration(milliseconds: 60),
                  ),
                  child: Container(
                    width: size.width * 0.78,
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Styles.AccentColor(context),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.add_rounded,
                            color: Styles.textColor(context), size: 20),
                        Text('ADD NEW CARD',
                            style: TextStyle(color: Styles.textColor(context)))
                      ],
                    ),
                  ),
                ),
                Container(
                  height: size.height * 0.74,
                  padding: EdgeInsets.all(18.0),
                  child: ListView.separated(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    controller: controller,
                    itemBuilder: (context, index) {
                      return MyCard(
                        acc: snapshot.data![index],
                      );
                    },
                    separatorBuilder: (context, _) => SizedBox(
                      height: 15.0,
                    ),
                    itemCount: snapshot.data!.length,
                  ),
                ),
              ],
            );
          }
          return Container(
            child: Center(child: CircularProgressIndicator()),
            color: Styles.bgColor(context),
          );
        },
      ),
    );
  }

  Future<List<BankAccounts>> loadAccounts(String orderBy) async {
    var accounts = await api.loadBankAccounts();
    sortList(orderBy, accounts);
    return accounts;
  }

  void sortList(String orderBy, List<BankAccounts> list) {
    switch (orderBy) {
      case "Balance":
        list.sort((first, second) {
          if ((first.balance < second.balance))
            return 1;
          else if ((first.balance > second.balance) & first.active)
            return -1;
          else
            return 0;
        });
        break;
      case "Type":
        list.sort((first, second) {
          if (first.type.type.compareTo(second.type.type)>0)
            return 1;
          else if (first.type.type.compareTo(second.type.type)<0)
            return -1;
          else
            return 0;
        });
        break;
      case "Inactive":
        list.sort((first, second) {
          if (second.active)
            return -1;
          else if (first.active)
            return 1;
          else
            return 0;
        });
        break;
    }
  }
}
