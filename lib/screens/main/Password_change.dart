import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../controller/editing_controller.dart';
import '../../services/api_dio.dart';
import '../../utils/styles.dart';
import '../../widgets/bottom_nav.dart';
import '../../widgets/my_button.dart';
import '../../widgets/my_password_field.dart';

class PasswordChange extends GetView<EditingController> {
  final String email;
  PasswordChange({Key? key, required this.email}) : super(key: key);
  final EditingController controller = Get.put(EditingController());
  final Api api = Api();
  @override
  Widget build(BuildContext context) {
    controller.email = email;
    return CustomScrollView(
      reverse: true,
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: ReactiveFormBuilder(
            form: () => controller.passwordChange,
            builder: (context, form, child) {
              return Scaffold(
                bottomSheet: Container(
                  color: Styles.bgColor(context),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  child: ReactiveFormConsumer(
                    builder: (context, form, child) {
                      return elevatedButton(
                        width: double.infinity,
                        color: Styles.selectedItemColor(context),
                        context: context,
                        callback: form.valid
                            ? () async{
                               String s=await changePassword(
                                    controller.passwordChange.value["newPassword"]
                                        .toString(),
                                    controller.passwordChange.value["password"]
                                        .toString());
                               if(s.contains("success")){
                                 ScaffoldMessenger.of(context).showSnackBar(
                                   SnackBar(
                                     content: Text(
                                       "Password changed successfully.",
                                       style: TextStyle(color: Colors.white),
                                     ),
                                     elevation: 1.0,
                                     backgroundColor: const Color(0xFF024751),
                                     duration: Duration(seconds: 3),
                                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)),
                                   ),
                                 );
                                 Get.offAll(
                                       () => BottomNav(),
                                   transition: Transition.rightToLeftWithFade,
                                   duration: Duration(
                                     milliseconds: 400,
                                   ),
                                 );
                               }
                              }
                            : null,
                        text: 'Confirm',
                      );
                    },
                  ),
                ),
                appBar: AppBar(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  leading: IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.keyboard_backspace_rounded,
                      size: 30.0,
                      color: Styles.whiteAndBlack(context),
                    ),
                  ),
                ),
                backgroundColor: Styles.bgColor(context),
                body: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 50,
                  ),
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    children: [
                      Text(
                        "Change your Password.",
                        style: TextStyle(
                          color: Styles.titleColor(context),
                          fontSize: 34,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 60,
                      ),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 0),
                              blurRadius: 0.01,
                              spreadRadius: 0.01,
                              color: Colors.black26,
                            ),
                          ],
                          color: Styles.AccentColor(context),
                          borderRadius: BorderRadius.circular(18),
                        ),
                        child: Column(
                          children: [
                            new MyPasswordField(
                              hintText: 'enter your current password.',
                              labelTxt: "Current Password *",
                              inputType: TextInputType.text,
                              controlName: "password",
                              validationMsgs: (control) => {
                                ValidationMessage.required:
                                    "This field must not be empty.",
                                ValidationMessage.minLength:
                                    "Password must be longer than 8 characters.",
                                ValidationMessage.pattern:
                                    "Please enter a valid password.",
                              },
                            ),
                            Divider(
                              color: Styles.dividerColor(context),
                              thickness: 0.8,
                            ),
                            new MyPasswordField(
                              hintText: 'enter your new password.',
                              labelTxt: "New Password *",
                              inputType: TextInputType.text,
                              controlName: "newPassword",
                              validationMsgs: (control) => {
                                ValidationMessage.required:
                                    "This field must not be empty.",
                                ValidationMessage.minLength:
                                    "Password must be longer than 8 characters.",
                                ValidationMessage.pattern:
                                    "Please enter a valid password.",
                              },
                            ),
                            Divider(
                              color: Styles.dividerColor(context),
                              thickness: 0.8,
                            ),
                            new MyPasswordField(
                              hintText: 'Confirm the new password.',
                              labelTxt: "Confirm Password *",
                              inputType: TextInputType.text,
                              controlName: "confirmPassword",
                              validationMsgs: (control) => {
                                ValidationMessage.required:
                                    "This field must not be empty.",
                                ValidationMessage.minLength:
                                    "Password must be longer than 8 characters.",
                                ValidationMessage.pattern:
                                    "Please enter a valid password.",
                                ValidationMessage.mustMatch:
                                    "Passwords must match."
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  Future<String> changePassword(String password, String oldPassword) {
    return api.changePassword(password, oldPassword);
  }
}
