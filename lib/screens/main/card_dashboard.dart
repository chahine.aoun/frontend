import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontend/screens/main/Transaction_screen.dart';
import 'package:frontend/services/api_dio.dart';
import 'package:frontend/widgets/bottom_nav.dart';
import 'package:frontend/widgets/card.dart';
import 'package:get/get.dart';

import '../../models/bankAccounts/bankAccounts.dart';
import '../../models/transactions/transaction.dart';
import '../../utils/styles.dart';
import '../../widgets/my_button.dart';
import '../../widgets/transaction_rep.dart';
import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CardDashboard extends StatefulWidget {
  final BankAccounts acc;
  final List<Transaction> ll;
  CardDashboard({
    Key? key,
    required this.acc,
    required this.ll,
  }) : super(key: key);
  Card createState() => Card();
}

class Card extends State<CardDashboard> {
  final Api api = Api();
  final List<String> dropDown = <String>["All", "Today", "Week", "Month"];
  String order = "All";
  List<Transaction> l = [];
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    l = getTransactions(order);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: widget.acc.active
            ? [
                IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        backgroundColor: Styles.AccentColor(context),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        content: Container(
                          height: 230,
                          width: 230,
                          child: FutureBuilder<String?>(
                            future: getInfo(),
                            builder: (BuildContext context,
                                AsyncSnapshot<String?> snapshot) {
                              if (snapshot.hasData) {
                                return QrImage(
                                  data: widget.acc.code.toString() +
                                      "/" +
                                      snapshot.data!,
                                  version: QrVersions.auto,
                                  size: 220,
                                  foregroundColor: Styles.titleColor(context),
                                );
                              }
                              return Container(
                                child: Center(child: CircularProgressIndicator()),
                                color: Styles.bgColor(context),
                              );
                            },
                          ),
                        ),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.qr_code_2_rounded,
                    size: 25.0,
                    color: Styles.whiteAndBlack(context),
                  ),
                ),
              ]
            : [
                IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        backgroundColor: Styles.AccentColor(context),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        content: Text(
                          "Are you sure you want to delete this account?",
                          textAlign: TextAlign.center,
                        ),
                        contentPadding: const EdgeInsets.all(30),
                        actions: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              elevatedButton(
                                color: Styles.selectedItemColor(context),
                                context: context,
                                text: "Confirm",
                                width: 120.0,
                                callback: () {
                                  deleteAccount();
                                  Navigator.of(context).pop();
                                  Get.offAll(() => BottomNav());
                                },
                              ),
                              elevatedButton(
                                color: Styles.selectedItemColor(context),
                                context: context,
                                width: 120.0,
                                text: 'Cancel',
                                callback: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.delete_rounded,
                    size: 25.0,
                    color: Styles.whiteAndBlack(context),
                  ),
                ),
              ],
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.keyboard_backspace_rounded,
            size: 30.0,
            color: Styles.whiteAndBlack(context),
          ),
        ),
      ),
      bottomSheet: Container(
        color: Styles.bgColor(context),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: elevatedButton(
          width: double.infinity,
          color: Styles.selectedItemColor(context),
          context: context,
          callback: widget.acc.active
              ? () {
                  Get.to(
                    () => TransactionScreen(
                      acc: widget.acc,
                    ),
                    transition: Transition.downToUp,
                    duration: Duration(
                      milliseconds: 250,
                    ),
                  );
                }
              : null,
          text: 'Send Money',
        ),
      ),
      backgroundColor: Styles.bgColor(context),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              height: Get.height * 0.25,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: MyCard(acc: widget.acc),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Styles.headerColor2(context),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Bank: ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                'Agency: ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                'Account Type: ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                'Interest Rate: ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              widget.acc.type.minBalance > 0
                                  ? Text(
                                      'Min Balance: ',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                              Text(
                                'Transaction Fee: ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              widget.acc.type.transactionLimit > 0
                                  ? Text(
                                      'Transaction Limit: ',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                              widget.acc.type.transactionLimit > 0
                                  ? Text(
                                      'Transaction Count: ',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.acc.bank,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                widget.acc.agency,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                widget.acc.type.type,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                (widget.acc.type.interestRate * 100)
                                        .toPrecision(1)
                                        .toString() +
                                    "%",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              widget.acc.type.minBalance > 0
                                  ? Text(
                                      "\$" +
                                          widget.acc.type.minBalance.toString(),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                              Text(
                                (widget.acc.type.transactionFee * 100)
                                        .toPrecision(1)
                                        .toString() +
                                    "%",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                              widget.acc.type.transactionLimit > 0
                                  ? Text(
                                      widget.acc.type.transactionLimit
                                          .toString(),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                              widget.acc.type.transactionLimit > 0
                                  ? Text(
                                      widget.acc.transactionCounter.toString(),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                      ),
                                    )
                                  : const SizedBox(),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: size.height * 0.15,
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                        color: Styles.AccentColor(context),
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Income',
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            '\$ ' +
                                getEarnings(widget.acc.code)[0]
                                    .toPrecision(3)
                                    .toString(),
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                          ),
                          const SizedBox(height: 8.0),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32.0, vertical: 4.0),
                            height: 40.0,
                            child: Sparkline(
                              cubicSmoothingFactor: 0.2,
                              useCubicSmoothing: true,
                              data: getEarnings(widget.acc.code).sublist(1),
                              lineWidth: 3.5,
                              lineColor: Color(0xFF58C1FE),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.15,
                      width: size.width * 0.4,
                      decoration: BoxDecoration(
                        color: Styles.AccentColor(context),
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Spending',
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            '\$ ' +
                                getSpending(widget.acc.code)[0]
                                    .toPrecision(3)
                                    .toString(),
                            style: TextStyle(
                              color: Styles.textColor(context),
                            ),
                          ),
                          const SizedBox(height: 8.0),
                          Container(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 32.0, vertical: 4.0),
                            height: 40.0,
                            child: Sparkline(
                              cubicSmoothingFactor: 0.2,
                              useCubicSmoothing: true,
                              data: getSpending(widget.acc.code).sublist(1),
                              lineWidth: 3.5,
                              lineColor: Color(0xFFEA5EB8),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 5.0,
                  ),
                  child: Text(
                    "Transactions",
                    style: TextStyle(
                      color: Styles.textColor(context),
                      fontSize: 14,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15.0,
                  ),
                  child: new DropdownButton<String>(
                    hint: Text(
                      order,
                      style: TextStyle(
                        color: Styles.textColor(context),
                        fontSize: 14,
                      ),
                    ),
                    borderRadius: BorderRadius.circular(20),
                    dropdownColor: Styles.AccentColor(context),
                    underline: Container(),
                    icon: Icon(Icons.arrow_drop_down_rounded,
                        color: Styles.whiteAndBlack(context)),
                    items:
                        dropDown.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (String? value) {
                      setState(
                        () {
                          order = value!;
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
            l.length == 0
                ? Padding(
                    padding: const EdgeInsets.all(50.0),
                    child: Text(
                      "No Transactions",
                      style: TextStyle(
                        color: Styles.textColor(context),
                        fontSize: 14,
                      ),
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 15.0,
                    ),
                    child: Container(
                      height: size.height * 0.332,
                      child: ListView.separated(
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          return TransactionRep(
                            amount: l[index].amount,
                            date: l[index].date,
                            icon: l[index].sent,
                            title: l[index].sent
                                ? l[index].receiver.toString()
                                : l[index].sender.toString(),
                          );
                        },
                        separatorBuilder: (context, _) => Divider(
                          indent: 100,
                          endIndent: 100,
                          color: Styles.dividerColor(context),
                          height: 1,
                          thickness: 0.5,
                        ),
                        itemCount: l.length,
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  List<double> getSpending(int code) {
    List<double> s = [0, 0];
    double total = 0;
    l.forEach((element) {
      if (element.sender == code) {
        s.insert(1, element.amount);
        total = total + element.amount;
      }
    });
    s.insert(0, total);
    return s;
  }

  List<double> getEarnings(int code) {
    double total = 0;
    List<double> e = [0, 0];
    l.forEach((element) {
      if (element.receiver == code) {
        e.insert(1, element.amount);
        total = total + element.amount;
      }
    });
    e.insert(0, total);
    return e;
  }

  void deleteAccount() {
    api.deleteBankAccount(widget.acc.code);
  }

  List<Transaction> getTransactions(String order) {
    List<Transaction> k = [];
    DateTime now = DateTime.now();
    switch (order) {
      case "Today":
        {
          k = [];
          widget.ll.forEach((element) {
            if (element.date.day == now.day) {
              k.insert(0, element);
            }
          });
        }
        break;

      case "Week":
        {
          k = [];
          widget.ll.forEach((element) {
            if (element.date.month == now.month &&
                element.date.year == now.year) {
              if (now.day <= 7) {
                if (element.date.day < 7) {
                  k.add(element);
                }
              } else if (now.day <= 14 && now.day >= 8) {
                if (element.date.day >= 8 && element.date.day <= 14) {
                  k.add(element);
                }
              } else if (now.day <= 21 && now.day >= 15) {
                if (element.date.day >= 15 && element.date.day <= 21) {
                  k.add(element);
                }
              } else if (now.day <= 28 && now.day >= 22) {
                if (element.date.day >= 22 && element.date.day <= 28) {
                  k.add(element);
                }
              } else {
                k.add(element);
              }
            }
          });
        }
        break;
      case "Month":
        {
          k = [];
          widget.ll.forEach((element) {
            if (element.date.month == now.month) {
              k.add(element);
            }
          });
        }
        break;

      default:
        {
          k = widget.ll;
        }
        break;
    }
    return k;
  }

  final _storage = const FlutterSecureStorage();

  Future<String?> getInfo() async {
    return await _storage.read(key: 'email');
  }
}
