import 'package:frontend/services/dio_client_authentication.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:reactive_image_picker/image_file.dart';

import '../services/api_dio.dart';

class FieldController extends GetxController {
  final DioClient _client = DioClient();
  final Api api=Api();
  final String email = "email";
  final String name = "name";
  final String lastName = "lastName";
  final String password = "password";
  final String confirmPassword = "confirmPassword";
  final String amount = "amount";
  final String pin = "pin";
  final String receiver = "receiver";
  late FormGroup loginFormGroup;
  late FormGroup fieldFormGroup;
  late FormGroup fieldFormGroup2;
  late FormGroup fieldFormGroup3;
  late FormGroup fieldFormGroup4;
  late FormGroup transactionFormGroup;
  final phonePattern = '< phone regex >';

  @override
  void onInit() {
    super.onInit();
    loginFormGroup = FormGroup(
      {
        email: FormControl<String>(
          validators: [
            Validators.required,
            Validators.composeOR([
              Validators.email,
              Validators.pattern(phonePattern),
            ]),
          ],
          asyncValidators: [existingEmail],
        ),

        password: FormControl<String>(
          validators: [
            Validators.required,
            Validators.minLength(8),
            Validators.pattern(
              RegExp(
                  r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$'),
            ),
          ],
        ),
      },
    );
    transactionFormGroup = FormGroup(
      {
        amount: FormControl<String>(
          validators: [
            Validators.required,
            Validators.pattern(RegExp(r'(^\d*\.?\d*)')),
          ],
        ),
        pin: FormControl<String>(
          validators: [
            Validators.required,
            Validators.number,
            Validators.maxLength(4),
            Validators.minLength(4),
          ],
          asyncValidators: [validPin],
        ),
        email: FormControl<String>(
          validators: [
            Validators.required,
            Validators.composeOR([
              Validators.email,
              Validators.pattern(phonePattern),
            ]),
          ],
          asyncValidators: [existingEmail],
        ),
       receiver: FormControl<String>(
          validators: [
            Validators.required,
            Validators.number,
            Validators.maxLength(9),
            Validators.minLength(9),
          ],
         asyncValidators: [validBankAccount],
        ),
      },
    );

    fieldFormGroup = FormGroup(
      {
        name: FormControl<String>(
          validators: [
            Validators.required,
            Validators.pattern(
              RegExp(r'^[a-zA-Z_ ]*$'),
            ),
          ],
        ),
        lastName: FormControl<String>(
          validators: [
            Validators.required,
            Validators.pattern(
              RegExp(r'^[a-zA-Z_ ]*$'),
            ),
          ],
        ),
        'gender': FormControl<String>(
          value: "male",
        ),
      },
    );
    fieldFormGroup2 = FormGroup({
      email: FormControl<String>(
        validators: [
          Validators.required,
          Validators.email,
        ],
        asyncValidators: [uniqueEmail],
      ),
      password: FormControl<String>(
        validators: [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            RegExp(
                r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$'),
          ),
        ],
      ),
      confirmPassword: FormControl<String>(
        validators: [
          Validators.required,
        ],
      ),
    }, validators: [
      Validators.mustMatch(password, confirmPassword),
    ]);
    fieldFormGroup3 = FormGroup(
      {
        'date': FormControl<DateTime>(
          validators: [
            Validators.required,
          ],
        ),
        'address': FormControl<String>(
          validators: [
            Validators.required,
            Validators.pattern(
              RegExp(r'[A-Za-z0-9\.\-\s\,]'),
            ),
          ],
        ),
        'countryOfResidence': FormControl<String>(
          value: "Tunisia",
        ),

      },
    );
    fieldFormGroup4 = FormGroup(
      {
        'date': FormControl<DateTime>(
          validators: [
            Validators.required,
          ],
        ),
        'idNumber': FormControl<String>(
          validators: [
            Validators.required,
            Validators.number,
            Validators.minLength(8),
          ],
        ),
        'dropdown': FormControl<bool>(
          value: null,
          validators: [
            Validators.required,
          ],
        ),
        'image': FormControl<ImageFile>(
          validators: [
            Validators.required,
          ],
        ),
        'referral': FormControl<String>(
          value: " ",
        ),
      },
    );
  }

  Future<Map<String, dynamic>?> uniqueEmail(AbstractControl<dynamic> control) async {
    final error = {"An account with this email already exists.": false};
    String otpMessage = await _client.emailTest(email: control.value);
    if (otpMessage == "true") {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
  Future<Map<String, dynamic>?> existingEmail(AbstractControl<dynamic> control) async {
    final error = {"There are no accounts linked to this email.": false};
    String otpMessage = await _client.emailTest(email: control.value);
    if (otpMessage != "true") {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
  Future<Map<String, dynamic>?> validBankAccount(AbstractControl<dynamic> control) async {
    final error = {"There are no accounts with this number.": false};
    String otpMessage = await api.checkBankAccount(control.value);
    if (otpMessage != "valid") {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
  Future<Map<String, dynamic>?> validPin(AbstractControl<dynamic> control) async {
    final error = {"You provided the wrong pin code.": false};
    String otpMessage = await api.checkPinCode(control.value);
    if (otpMessage != "correct") {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
}
