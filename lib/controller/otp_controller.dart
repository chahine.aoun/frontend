import 'package:frontend/services/dio_client_authentication.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

class OtpController extends GetxController {
  final String otpControl = "otpForm";
  late FormGroup otpForm;
  late String number;
  final DioClient _client = DioClient();
  late var otpMessage;

  @override
  void onInit() {
    super.onInit();
    otpForm = FormGroup({
      otpControl: FormControl<String>(
        validators: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(6),
        ],
        asyncValidators: [validCode],
        asyncValidatorsDebounceTime: 1000,
      ),
    });
  }

  Future<Map<String, dynamic>?> validCode(
      AbstractControl<dynamic> control) async {
    final error = {"wrong": false};
    otpMessage =
        await _client.VerifyOtp(phoneNumber: number, code: control.value);
    final _validCode = await Future.delayed(
      Duration(seconds: 2),
      () => otpMessage.compareTo("Confirmed successfully")==0 ,
    );
    if (!_validCode) {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
}
