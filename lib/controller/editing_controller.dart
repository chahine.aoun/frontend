
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:reactive_image_picker/image_file.dart';

import '../services/api_dio.dart';
import '../services/dio_client_authentication.dart';

class EditingController extends GetxController {
  final Api api=Api();
  final DioClient _client = DioClient();
  late FormGroup editingForm;
  final String editingControl = "editingForm";
  late FormGroup passwordChange ;

 String email ="";
  final String name = "name";
  final String lastName = "lastName";
  final String password = "password";
  final String newPassword = "newPassword";
  final String confirmPassword = "confirmPassword";


  final phonePattern = '< phone regex >';
  @override
  void onInit() {
    super.onInit();
    editingForm = FormGroup({
      name: FormControl<String>(
        validators: [
          Validators.required,
          Validators.pattern(
            RegExp(r'^[a-zA-Z_ ]*$'),
          ),
        ],
      ),
      lastName: FormControl<String>(
        validators: [
          Validators.required,
          Validators.pattern(
            RegExp(r'^[a-zA-Z_ ]*$'),
          ),
        ],
      ),
      'date': FormControl<DateTime>(
        validators: [
          Validators.required,
        ],
      ),
      'address': FormControl<String>(
        validators: [
          Validators.required,
          Validators.pattern(
            RegExp(r'[A-Za-z0-9\.\-\s\,]'),
          ),
        ],
      ),
      'countryOfResidence': FormControl<String>(
        value: "Tunisia",
      ),
      'expirationDate': FormControl<DateTime>(
        validators: [
          Validators.required,
        ],
      ),
      'idNumber': FormControl<String>(
        validators: [
          Validators.required,
          Validators.number,
          Validators.minLength(8),
        ],
      ),
      'dropdown': FormControl<bool>(
        value: null,
        validators: [
          Validators.required,
        ],
      ),
      'image': FormControl<ImageFile>(
      ),
      'referral': FormControl<String>(
        value: " ",
      ),
    });

    passwordChange = FormGroup({
      password: FormControl<String>(
        validators: [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            RegExp(
                r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$'),
          ),
        ],
        asyncValidators: [correctPassword],
      ),

      newPassword: FormControl<String>(
        validators: [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            RegExp(
                r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$'),
          ),
        ],
        asyncValidators: [correctNewPassword],
      ),

      confirmPassword: FormControl<String>(
        validators: [
          Validators.required,
        ],
      ),
    }, validators: [
      Validators.mustMatch(newPassword, confirmPassword),
    ]);
  }
  Future<Map<String, dynamic>?> correctPassword(AbstractControl<dynamic> control) async {
    final error = {"Wrong password.": false};
    String otpMessage = await _client.login(login:email, pass: control.value);

    if (otpMessage != "null") {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
  Future<Map<String, dynamic>?> correctNewPassword(AbstractControl<dynamic> control) async {
    final error = {"New password must be different from the last one.": false};
    if (passwordChange.value["newPassword"].toString().compareTo(passwordChange.value["password"].toString())==0) {
      control.markAllAsTouched();
      return error;
    }
    return null;
  }
}
